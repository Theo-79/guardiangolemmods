package fr.welsymc.guardiangolem.proxy;

import cpw.mods.fml.client.registry.RenderingRegistry;
import fr.welsymc.guardiangolem.client.renderer.ModelGolem;
import fr.welsymc.guardiangolem.client.renderer.RenderGolem;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;


public class ClientProxy
  extends CommonProxy
{
  public static boolean hasWither = false;
  
  public ClientProxy() {}
  
  public void registerRenderer() { super.registerRenderer();
    RenderingRegistry.registerEntityRenderingHandler(EntityGolem.class, new RenderGolem(new ModelGolem(), 0.5F));
  }
}
