package fr.welsymc.guardiangolem.client.gui.components;

import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.client.Utils;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.items.guardianupgrades.GuardianUpgradeItem;
import fr.welsymc.guardiangolem.common.network.PacketSendInstructions;
import fr.welsymc.guardiangolem.common.utils.GuiHelper;
import fr.welsymc.guardiangolem.common.utils.LibRessources;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class GuiTreeItem extends Gui
{
  ResourceLocation background = new ResourceLocation(LibRessources.GUI_ELEMENTS);
  private RenderItem itemRenderer = new RenderItem();
  
  public GuardianUpgradeItem item;
  
  int posX;
  
  int posY;
  EntityGolem guardian;
  Minecraft mc;
  FontRenderer fr;
  long lastClick;
  boolean hover;
  
  public GuiTreeItem(EntityGolem guardian, GuardianUpgradeItem item, int posX, int posY)
  {
    this.item = item;
    this.posX = posX;
    this.posY = posY;
    this.guardian = guardian;
    this.mc = Minecraft.getMinecraft();
    this.fr = this.mc.fontRenderer;
  }
  
  public void draw(int x, int y, RenderItem itemRenderer) {
    for (GuardianUpgradeItem itemUpgrade : this.item.getParents()) {
      GL11.glPushMatrix();
      GL11.glTranslated(0.0D, 0.0D, 1.0D);
      int center = 9;
      center = 13;
      float b;
      float r; float g; float b1; if (this.guardian.getUpgradeLevel(this.item.getId()) == 0) {
        r = 0.0F;
        g = 0.0F;
        b1 = 0.0F;
      } else {
        r = 1.0F;
        g = 1.0F;
        b1 = 1.0F;
      }
      GuiHelper.drawLine(this.posX - x + center, this.posY - y + center, itemUpgrade.getX() - x + center, itemUpgrade
        .getY() - y + center, r, g, b1);
      GL11.glPopMatrix();
    }
    this.mc.renderEngine.bindTexture(this.background);
    GL11.glPushMatrix();
    GL11.glDisable(2896);
    GL11.glTranslated(this.posX - x, this.posY - y, 10.0D);
    GL11.glScaled(1.2D, 1.2D, 1.0D);
    if (this.guardian.getUpgradeLevel(this.item.getId()) == 0) {
      GL11.glColor3f(0.3F, 0.3F, 0.3F);
    }
    drawTexturedModalRect(0, 0, LibRessources.X_UNIFIED_BUTTON_1, LibRessources.Y_UNIFIED_BUTTON_1, 22, 22);
    
    GL11.glPushMatrix();
    GL11.glTranslated(5.0D, 4.0D, 10.0D);
    GL11.glScaled(0.8D, 0.8D, 1.0D);
    RenderHelper.enableGUIStandardItemLighting();
    GL11.glDisable(2896);
    GL11.glEnable(32826);
    GL11.glEnable(2903);
    GL11.glEnable(2896);
    GL11.glTranslated(0.0D, 0.0D, 900.0D);
    GL11.glDepthMask(true);
    itemRenderer.renderItemAndEffectIntoGUI(this.mc.fontRenderer, this.mc.renderEngine, this.item.getItemStack(0), 0, 0);
    
    GL11.glDisable(2896);
    GL11.glPopMatrix();
    GL11.glPopMatrix();
  }
  
  public void click(int mouseX, int mouseY)
  {
    if ((this.hover) && (Mouse.isButtonDown(0)) && (System.currentTimeMillis() - this.lastClick > 250L)) {
      this.lastClick = System.currentTimeMillis();
      if (this.guardian.addUpgrade(this.item.getId())) {
        if (this.guardian != null) {
          if ((this.item.getId() == 28) && (!this.guardian.hasUpgrade(28)))
          {
            this.guardian.targetTasks.removeTask(this.guardian.entityAIGuardian);
            this.guardian.targetTasks.removeTask(this.guardian.entityAISwimming);
            
            this.guardian.targetTasks.removeTask(this.guardian.entityAIMoveTowardsRestriction);
            this.guardian.targetTasks.removeTask(this.guardian.entityAIWander);
            this.guardian.targetTasks.removeTask(this.guardian.entityAIWatchClosest);
            this.guardian.targetTasks.removeTask(this.guardian.entityAILookIdle);
            this.guardian.targetTasks.removeTask(this.guardian.entityAIAttackPlayer);
            this.guardian.targetTasks.removeTask(this.guardian.entityAIAttackWither);
            this.guardian.targetTasks.removeTask(this.guardian.entityAIAttackGolem);
            
            this.guardian.isFarmer = true;
          }
          GuardianGolem.network.sendToServer(new PacketSendInstructions(90, new String[] {this.item
            .getId() + "", this.guardian.getEntityId() + "" }));
        }
        
      }
      else if ((this.item.getId() == 28) && (this.guardian.getUpgradeLevel(28) != 0)) {
        GuardianGolem.network.sendToServer(new PacketSendInstructions(100, new String[] {this.guardian
          .getEntityId() + "" }));
      }
    }
  }
  
  public void drawHover(int mouseX, int mouseY, int x, int y)
  {
    this.hover = ((mouseX > this.posX - x) && (mouseX < this.posX - x + 26.4D) && (mouseY > this.posY - y) && (mouseY < this.posY - y + 26.4D));
    



    ArrayList<String> list = new ArrayList();
    list.add(EnumChatFormatting.RED + "" + EnumChatFormatting.BOLD + I18n.format(this.item.getName(), new Object[0]));
    String[] desc = I18n.format(this.item.getDesc(), new Object[0]).split("<br>");
    for (String s : desc) {
      list.add(s);
    }
    list.add(EnumChatFormatting.BOLD + I18n.format("misc.level", new Object[0]) + ": " + EnumChatFormatting.WHITE + this.guardian
      .getUpgradeLevel(this.item.getId()) + "/" + this.item.getMaxLevel());
    if (this.hover) {
      Utils.INSTANCE.drawHoveringText(list, mouseX, mouseY, this.mc.displayWidth, this.mc.displayHeight, -1, this.fr);
    }
  }
}
