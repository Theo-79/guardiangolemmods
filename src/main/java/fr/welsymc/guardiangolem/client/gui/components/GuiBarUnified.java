package fr.welsymc.guardiangolem.client.gui.components;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.utils.LibRessources;
import fr.welsymc.guardiangolem.proxy.ClientProxy;
import java.awt.Color;
import java.util.Iterator;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;
import scala.actors.threadpool.Arrays;



public class GuiBarUnified
  extends Gui
{
  int type;
  int xPos;
  int yPos;
  int xOffset;
  int yOffset;
  boolean shouldHover;
  int width;
  int scaledValue;
  int scaledMax;
  double size;
  boolean hover;
  Minecraft mc;
  FontRenderer fr;
  ResourceLocation location;
  ResourceLocation rainbowLocation;
  
  public GuiBarUnified(int x, int y, int width, int type, double size, int xOffset, int yOffset)
  {
    this(x, y, width, type, size);
    this.shouldHover = true;
    this.xOffset = xOffset;
    this.yOffset = yOffset;
  }
  
  public GuiBarUnified(int x, int y, int width, int type, double size) {
    this.xPos = x;
    this.yPos = y;
    this.width = width;
    this.type = type;
    this.size = size;
    this.shouldHover = true;
    this.mc = Minecraft.getMinecraft();
    this.fr = this.mc.fontRenderer;
    this.location = new ResourceLocation(LibRessources.GUI_UNIFIED_BAR);
    this.rainbowLocation = new ResourceLocation(LibRessources.GUI_UNIFIED_BAR_RAINBOW);
  }
  
  public void setScaledBar(int value, int max) {
    this.scaledValue = value;
    this.scaledMax = max;
  }
  
  public void setPosition(int x, int y) {
    this.xPos = x;
    this.yPos = y;
  }
  
  public void setType(int type) {
    this.type = type;
  }
  



  public void drawBar(boolean rainbow)
  {
    AxisAlignedBB axisalignedbb = AxisAlignedBB.getBoundingBox(this.mc.thePlayer.posX, this.mc.thePlayer.posY, this.mc.thePlayer.posZ, this.mc.thePlayer.posX + 1.0D, this.mc.thePlayer.posY + 1.0D, this.mc.thePlayer.posZ + 1.0D).expand(32.0D, 32.0D, 32.0D);
    
    if (System.currentTimeMillis() % 200L < 10L)
    {
      EntityWither boss = null;
      
      for (Object entity : this.mc.thePlayer.worldObj.getEntitiesWithinAABB(EntityWither.class, axisalignedbb)) {
        if ((entity instanceof EntityWither)) {
          boss = (EntityWither)entity;
        }
      }
      ClientProxy.hasWither = boss != null;
    }
    
    if (this.mc.thePlayer.worldObj.getEntitiesWithinAABB(EntityGolem.class, axisalignedbb).size() <= 0) {
      return;
    }
    
    if (ClientProxy.hasWither) {
      return;
    }
    
    this.mc.renderEngine.bindTexture(rainbow ? this.rainbowLocation : this.location);
    
    GL11.glPushMatrix();
    GL11.glScaled(this.size, this.size, 0.0D);
    if (rainbow) {
      int c = Color.HSBtoRGB((float)(System.currentTimeMillis() % 3000L) / 3000.0F, 0.8F, 0.8F);
      Color color = new Color(c);
      GL11.glColor3f(color.getRed() / 255.0F, color.getGreen() / 255.0F, color
        .getBlue() / 255.0F);
    }
    drawTexturedModalRect(this.xPos, this.yPos, 0, this.type, 1, LibRessources.HEIGHT_UNIFIED_BAR);
    
    for (int i = 0; i < this.width - 2; i++) {
      drawTexturedModalRect(this.xPos + 1 + i, this.yPos, 1, this.type, 2, LibRessources.HEIGHT_UNIFIED_BAR);
    }
    
    drawTexturedModalRect(this.xPos + this.width - 1, this.yPos, 0, this.type, 1, LibRessources.HEIGHT_UNIFIED_BAR);
    
    for (int i = 0; i < this.scaledValue / this.scaledMax * (this.width - 2); i++) {
      drawTexturedModalRect(this.xPos + 1 + i, this.yPos + 1, 2, this.type, 3, LibRessources.HEIGHT_UNIFIED_BAR);
    }
    GL11.glPopMatrix();
  }
  
  public double getSize() {
    return this.size;
  }
  
  public void drawHover(int mouseX, int mouseY, int centerX, int centerY) {
    int x = this.xPos + centerX + this.xOffset;
    int y = this.yPos + centerY + this.yOffset;
    this.hover = ((mouseX > x) && (mouseX < x + this.size * this.width) && (mouseY > y) && (mouseY < y + this.size * LibRessources.HEIGHT_UNIFIED_BAR));
    
    if (!this.shouldHover)
      return;
    if (this.hover) {
      drawHoveringText(Arrays.asList(new String[] { this.scaledValue + "/" + this.scaledMax }), mouseX / 2, mouseY, this.fr);
    }
  }
  

  public void drawHoveringText(List p_146283_1_, int p_146283_2_, int p_146283_3_, FontRenderer font)
  {
    if (!p_146283_1_.isEmpty())
    {
      GL11.glPushMatrix();
      GL11.glDisable(32826);
      RenderHelper.disableStandardItemLighting();
      GL11.glDisable(2896);
      GL11.glDisable(2929);
      GL11.glTranslated(p_146283_2_, p_146283_3_, this.zLevel);
      int k = 0;
      Iterator iterator = p_146283_1_.iterator();
      
      while (iterator.hasNext())
      {
        String s = (String)iterator.next();
        int l = font.getStringWidth(s);
        
        if (l > k)
        {
          k = l;
        }
      }
      
      int j2 = p_146283_2_ - 64;
      int k2 = p_146283_3_ - 12;
      int i1 = 8;
      
      if (p_146283_1_.size() > 1)
      {
        i1 += 2 + (p_146283_1_.size() - 1) * 10;
      }
      

      if (k2 + i1 + 6 > LibRessources.HEIGHT_UNIFIED_BAR)
      {
        k2 = LibRessources.HEIGHT_UNIFIED_BAR - i1 - 6;
      }
      
      this.zLevel = 300.0F;
      int j1 = -267386864;
      drawGradientRect(j2 - 3, k2 - 4, j2 + k + 3, k2 - 3, j1, j1);
      drawGradientRect(j2 - 3, k2 + i1 + 3, j2 + k + 3, k2 + i1 + 4, j1, j1);
      drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 + i1 + 3, j1, j1);
      drawGradientRect(j2 - 4, k2 - 3, j2 - 3, k2 + i1 + 3, j1, j1);
      drawGradientRect(j2 + k + 3, k2 - 3, j2 + k + 4, k2 + i1 + 3, j1, j1);
      int k1 = 1347420415;
      int l1 = (k1 & 0xFEFEFE) >> 1 | k1 & 0xFF000000;
      drawGradientRect(j2 - 3, k2 - 3 + 1, j2 - 3 + 1, k2 + i1 + 3 - 1, k1, l1);
      drawGradientRect(j2 + k + 2, k2 - 3 + 1, j2 + k + 3, k2 + i1 + 3 - 1, k1, l1);
      drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 - 3 + 1, k1, k1);
      drawGradientRect(j2 - 3, k2 + i1 + 2, j2 + k + 3, k2 + i1 + 3, l1, l1);
      
      for (int i2 = 0; i2 < p_146283_1_.size(); i2++)
      {
        String s1 = (String)p_146283_1_.get(i2);
        font.drawStringWithShadow(s1, j2, k2, -1);
        
        if (i2 == 0)
        {
          k2 += 2;
        }
        
        k2 += 10;
      }
      
      this.zLevel = 0.0F;
      GL11.glEnable(2896);
      GL11.glEnable(2929);
      RenderHelper.enableStandardItemLighting();
      GL11.glEnable(32826);
      GL11.glPopMatrix();
    }
  }
}
