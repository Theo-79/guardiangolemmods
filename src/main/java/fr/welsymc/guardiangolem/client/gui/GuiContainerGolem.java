package fr.welsymc.guardiangolem.client.gui;

import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.client.gui.components.GuiBarUnified;
import fr.welsymc.guardiangolem.client.gui.components.GuiButtonTab;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.network.CSOpenTree;
import fr.welsymc.guardiangolem.common.utils.GuardianUtils;
import fr.welsymc.guardiangolem.common.utils.LibRessources;
import java.awt.Color;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiContainerGolem
  extends GuiContainer
{
  private ResourceLocation back = new ResourceLocation("guardiangolem", "textures/gui/golem_GUI.png");
  private EntityGolem entity;
  GuiBarUnified bar;
  
  public void initGui()
  {
    super.initGui();
    int posX = (this.width - LibRessources.WIDTH_GUARDIAN_GOLEM_GUI) / 2;
    int posY = (this.height - LibRessources.HEIGHT_GUARDUAN_GOLEM_GUI) / 2;
    
    this.bar = new GuiBarUnified(6, 12, 145, LibRessources.UNIFIED_BAR_GREEN, 1.5D);
    
    this.buttonList.add(new GuiButtonTab(0, posX - 22 - 4, posY, new ItemStack(Items.apple), 
      I18n.format("guardian.gui.tree", new Object[0])));
  }
  


  protected void actionPerformed(GuiButton b)
  {
    if (b.id == 0)
    {
      GuardianGolem.network.sendToServer(new CSOpenTree(1, this.entity.getEntityId()));
    }
    super.actionPerformed(b);
  }
  
  public GuiContainerGolem(Container p_i1072_1_, EntityGolem ent)
  {
    super(p_i1072_1_);
    this.xSize = 238;
    this.ySize = 227;
    this.entity = ent;
  }
  
  protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_)
  {
    GL11.glPushMatrix();
    GL11.glColor4d(1.0D, 1.0D, 1.0D, 1.0D);
    Minecraft.getMinecraft().getTextureManager().bindTexture(this.back);
    
    int x = (this.width - this.xSize) / 2;
    int y = (this.height - this.ySize) / 2;
    drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
    GL11.glPopMatrix();
  }
  

  protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
  {
    GL11.glPushMatrix();
    GL11.glColor4d(1.0D, 1.0D, 1.0D, 1.0D);
    drawCenteredString(this.mc.fontRenderer, "Guardian Golem (" + this.entity.getLastKnownOwnerName() + ")", this.xSize / 2, 6, new Color(255, 0, 0).getRGB());
    drawCenteredString(this.mc.fontRenderer, (int)this.entity.getEntityAttribute(SharedMonsterAttributes.attackDamage).getBaseValue() + "", 92, 130, 16777215);
    double percentage = this.entity.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getBaseValue() * 100.0D;
    
    drawCenteredString(this.mc.fontRenderer, (int)percentage + "%", 128, 130, 16777215);
    drawCenteredString(this.mc.fontRenderer, this.entity.getEntityAttribute(SharedMonsterAttributes.maxHealth).getBaseValue() + "", 174, 130, 16777215);
    this.bar.setScaledBar(this.entity.getLevel() >= 80 ? GuardianUtils.getXPForLevel(this.entity.getLevel() + 1) : this.entity.getSubLevel(), 
      GuardianUtils.getXPForLevel(this.entity.getLevel() + 1));
    this.bar.drawBar(this.entity.getLevel() >= 80);
    if (this.entity.getLevel() >= 80) {
      drawCenteredString(this.mc.fontRenderer, "Niveau " + this.entity.getLevel() + " (Niveau max)", this.xSize / 2, 20, 16777215);
    } else {
      drawCenteredString(this.mc.fontRenderer, "Niveau " + this.entity.getLevel() + " (" + this.entity.getSubLevel() + "/" + GuardianUtils.getXPForLevel(this.entity.getLevel() + 1) + ")", this.xSize / 2, 20, 16777215);
    }
    drawCenteredString(this.mc.fontRenderer, "Whitelist", 108, 40, 16777215);
    drawCenteredString(this.mc.fontRenderer, "Cosmetic", 108, 77, 16777215);
    

    GL11.glColor4d(1.0D, 1.0D, 1.0D, 1.0D);
    RenderItem.getInstance().renderItemAndEffectIntoGUI(this.mc.fontRenderer, this.mc.getTextureManager(), new ItemStack(Items.diamond_sword), 84, 110);
    RenderItem.getInstance().renderItemAndEffectIntoGUI(this.mc.fontRenderer, this.mc.getTextureManager(), new ItemStack(Items.feather), 120, 110);
    
    EntityLiving e = this.entity;
    e.setCustomNameTag("");
    GuiInventory.func_147046_a(43, 132, 35, 35.0F, 0.0625F, e);
    GL11.glPopMatrix();
    GL11.glPushMatrix();
    this.mc.getTextureManager().bindTexture(new ResourceLocation("textures/gui/icons.png"));
    GL11.glColor4d(1.0D, 1.0D, 1.0D, 1.0D);
    GL11.glDisable(2896);
    GL11.glEnable(3042);
    GL11.glTranslated(166.0D, 112.0D, 1.0D);
    GL11.glScaled(1.5D, 1.5D, 0.0D);
    drawTexturedModalRect(0, 0, 16, 0, 9, 9);
    drawTexturedModalRect(0, 0, 52, 0, 9, 9);
    GL11.glPopMatrix();
  }
}
