package fr.welsymc.guardiangolem.client.gui.components;

import fr.welsymc.guardiangolem.common.utils.LibRessources;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;





public class GuiButtonTab
  extends GuiButton
{
  int style;
  ItemStack item;
  ResourceLocation guiElement = new ResourceLocation(LibRessources.GUI_ELEMENTS);
  private RenderItem renderItem = new RenderItem();
  FontRenderer fr;
  String name;
  boolean hovered;
  
  public GuiButtonTab(int buttonId, int x, int y, ItemStack item, String name) {
    this(buttonId, x, y, 0, item, name);
  }
  
  public GuiButtonTab(int buttonId, int x, int y, int style, ItemStack item, String name) {
    super(buttonId, x, y, 22, 22, "");
    this.style = style;
    this.item = item;
    this.fr = Minecraft.getMinecraft().fontRenderer;
    this.name = name;
  }
  
  public void drawButton(Minecraft mc, int mouseX, int mouseY)
  {
    if (this.visible) {
      FontRenderer fontrenderer = mc.fontRenderer;
      mc.getTextureManager().bindTexture(this.guiElement);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.hovered = ((mouseX >= this.xPosition) && (mouseY >= this.yPosition) && (mouseX < this.xPosition + this.width) && (mouseY < this.yPosition + this.height));
      
      int i = getHoverState(this.hovered);
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      
      if (!this.hovered) {
        drawTexturedModalRect(this.xPosition, this.yPosition, LibRessources.X_UNIFIED_BUTTON_1, LibRessources.Y_UNIFIED_BUTTON_1, 22, 22);
      }
      else
      {
        drawTexturedModalRect(this.xPosition, this.yPosition, LibRessources.X_UNIFIED_BUTTON_1, LibRessources.Y_UNIFIED_BUTTON_1 + 22, 22, 22);
      }
      
      GL11.glPushMatrix();
      GL11.glDisable(2896);
      GL11.glTranslated(this.xPosition, this.yPosition, 10.0D);
      GL11.glPushMatrix();
      GL11.glTranslated(5.0D, 4.0D, 0.0D);
      GL11.glScaled(0.8D, 0.8D, 1.0D);
      GL11.glEnable(3553);
      this.renderItem.renderItemAndEffectIntoGUI(this.fr, mc.getTextureManager(), this.item, 0, 0);
      GL11.glPopMatrix();
      GL11.glPopMatrix();
      
      mouseDragged(mc, mouseX, mouseY);
      int j = 14737632;
      
      if (this.packedFGColour != 0) {
        j = this.packedFGColour;
      } else if (!this.enabled) {
        j = 10526880;
      } else if (this.hovered) {
        j = 16777120;
      }
      
      drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, j);
    }
    
    if ((this.hovered) && 
      (this.hovered)) {
      ArrayList<String> list = new ArrayList();
      list.add(this.name);
    }
  }
  


  public boolean getHovered()
  {
    return this.hovered;
  }
}
