package fr.welsymc.guardiangolem.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;

public class GuiRenameGolem extends GuiScreen
{
  public GuiTextField nameField;
  public String name;
  
  public GuiRenameGolem(String name)
  {
    this.name = name;
  }
  
  public void initGui()
  {
    this.nameField = new GuiTextField(this.mc.fontRenderer, this.width / 2 - 100, this.height / 2 - 10, 200, 20);
    this.nameField.setMaxStringLength(48);
    this.nameField.setText(this.name);
    super.initGui();
  }
  

  public boolean doesGuiPauseGame()
  {
    return false;
  }
  
  public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_)
  {
    drawDefaultBackground();
    this.nameField.drawTextBox();
    super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
  }
  
  public void onGuiClosed()
  {
    if (Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem() != null)
    {
      if ((Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem().getItem() instanceof fr.welsymc.guardiangolem.common.items.ItemGuardianRenamer)) {
        fr.welsymc.guardiangolem.GuardianGolem.network.sendToServer(new fr.welsymc.guardiangolem.common.network.CSRenameGolemItem(this.name));
        
        net.minecraft.nbt.NBTTagCompound tag = null;
        if (Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem().hasTagCompound()) {
          tag = Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem().getTagCompound();
        } else {
          tag = new net.minecraft.nbt.NBTTagCompound();
        }
        
        tag.setString("golemName", this.name);
        Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem().setTagCompound(tag);
      } }
    super.onGuiClosed();
  }
  


  protected void keyTyped(char p_73869_1_, int p_73869_2_)
  {
    if (this.nameField.isFocused()) {
      if (p_73869_2_ == 1) { super.keyTyped(p_73869_1_, p_73869_2_);
      }
      this.nameField.textboxKeyTyped(p_73869_1_, p_73869_2_);
    } else {
      super.keyTyped(p_73869_1_, p_73869_2_);
    }
    this.name = this.nameField.getText().replace('�', '�');
  }
  
  protected void mouseClicked(int p_73864_1_, int p_73864_2_, int p_73864_3_)
  {
    this.nameField.mouseClicked(p_73864_1_, p_73864_2_, p_73864_3_);
    super.mouseClicked(p_73864_1_, p_73864_2_, p_73864_3_);
  }
  

  public void updateScreen()
  {
    if (this.nameField.isFocused()) {
      this.nameField.updateCursorCounter();
    }
    super.updateScreen();
  }
}
