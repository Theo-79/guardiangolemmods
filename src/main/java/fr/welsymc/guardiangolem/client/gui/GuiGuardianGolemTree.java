package fr.welsymc.guardiangolem.client.gui;

import java.util.ArrayList;
import java.util.Map;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.client.gui.components.GuiButtonTab;
import fr.welsymc.guardiangolem.client.gui.components.GuiTreeItem;
import fr.welsymc.guardiangolem.common.containers.ContainerGolem;
import fr.welsymc.guardiangolem.common.items.guardianupgrades.GuardianUpgradeItem;
import fr.welsymc.guardiangolem.common.items.guardianupgrades.ModGuardianUpgrades;
import fr.welsymc.guardiangolem.common.network.CSOpenTree;
import fr.welsymc.guardiangolem.common.utils.LibRessources;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiGuardianGolemTree extends GuiContainer
{
  ResourceLocation background = new ResourceLocation(LibRessources.GUARDIAN_GOLEM_GUI_TREE);
  Minecraft mc;
  FontRenderer fr;
  ContainerGolem container;
  int currentX;
  int currentY;
  int lastX;
  int lastY;
  boolean isDown;
  boolean hover;
  boolean initialized;
  ArrayList<GuiTreeItem> itemList;
  
  public GuiGuardianGolemTree(ContainerGolem container) {
    super(container);
    this.mc = Minecraft.getMinecraft();
    this.fr = this.mc.fontRenderer;
    this.container = container;
    this.currentX = 0;
    this.currentY = 0;
    this.itemList = new ArrayList();
  }
  
  public void initGui()
  {
    super.initGui();
    
    this.guiLeft = 0;
    this.guiTop = 0;
    
    this.itemList.clear();
    this.buttonList.add(new GuiButtonTab(0, 4, 4, new ItemStack(Items.paper), 
      I18n.format("guardian.name", new Object[0])));
    for (Map.Entry<Integer, GuardianUpgradeItem> e : ModGuardianUpgrades.list.entrySet()) {
      this.itemList.add(new GuiTreeItem(this.container.getGolem(), (GuardianUpgradeItem)e.getValue(), ((GuardianUpgradeItem)e.getValue()).getX(), ((GuardianUpgradeItem)e.getValue()).getY()));
    }
    
    this.currentX = 150;
    this.currentY = 150;
  }
  
  public void drawScreen(int mouseX, int mouseY, float partialTicks)
  {
    this.mc.renderEngine.bindTexture(this.background);
    GL11.glPushMatrix();
    GL11.glTranslated(0.0D, 0.0D, 1.0D);
    GL11.glScaled(0.001953125D * this.width * 2.0D, 0.001953125D * this.height * 2.0D, 1.0D);
    
    drawTexturedModalRect(0, 0, 0, 0, 512, 512);
    GL11.glPopMatrix();
    if (Mouse.isButtonDown(0)) {
      if (this.isDown) {
        this.currentX += this.lastX - mouseX;
        this.currentY += this.lastY - mouseY;
      }
      this.isDown = true;
    } else {
      this.isDown = false; }
    this.lastX = mouseX;
    this.lastY = mouseY;
    for (GuiTreeItem item : this.itemList)
    {
      item.draw(this.currentX, this.currentY, itemRender);
      item.click(mouseX, mouseY);
    }
    
    super.drawScreen(mouseX, mouseY, partialTicks);
  }
  


  public void hoverDraw() {}
  

  protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
  {
    String capacityPoints = I18n.format("guardian.capacitypoints", new Object[0]) + ": " + this.container.getGolem().getCapacityPoints();
    GL11.glPushMatrix();
    GL11.glTranslated(this.width - this.fr.getStringWidth(capacityPoints) - 3, this.height - 13, 20.0D);
    
    this.fr.drawStringWithShadow(capacityPoints, 0, 0, 16777215);
    GL11.glPopMatrix();
  }
  

  protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
  {
    for (GuiTreeItem item : this.itemList) {
      item.drawHover(mouseX, mouseY, this.currentX, this.currentY);
    }
  }
  
  protected void actionPerformed(GuiButton button)
  {
    super.actionPerformed(button);
    switch (button.id) {
    case 0: 
      GuardianGolem.network.sendToServer(new CSOpenTree(0, this.container.getGolem().getEntityId()));
    }
  }
}
