package fr.welsymc.guardiangolem.client.gui;

import fr.welsymc.guardiangolem.common.containers.ContainerGuardianKeeper;
import fr.welsymc.guardiangolem.common.utils.GuardianUtils;
import fr.welsymc.guardiangolem.common.utils.LibRessources;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

public class GuiGuardianKeeper extends GuiContainer
{
  ResourceLocation background = GuardianUtils.GUARDIAN_KEEPER_GUI;
  Minecraft mc;
  ContainerGuardianKeeper container;
  FontRenderer fr;
  int posX;
  int posY;
  
  public GuiGuardianKeeper(ContainerGuardianKeeper container)
  {
    super(container);
    this.mc = Minecraft.getMinecraft();
    this.container = container;
    this.fr = this.mc.fontRenderer;
  }
  
  public void initGui()
  {
    super.initGui();
    
    this.posX = ((this.width - LibRessources.WIDTH_GUARDIAN_KEEPER_GUI) / 2);
    this.posY = ((this.height - LibRessources.HEIGHT_GUARDUAN_KEEPER_GUI) / 2);
  }
  

  protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
  {
    this.mc.renderEngine.bindTexture(this.background);
    drawTexturedModalRect(this.posX, this.posY, 0, 0, LibRessources.WIDTH_GUARDIAN_KEEPER_GUI, LibRessources.HEIGHT_GUARDUAN_KEEPER_GUI);
  }
  
  protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {}
}
