package fr.welsymc.guardiangolem.client;

import java.util.Iterator;
import java.util.List;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.RenderHelper;
import org.lwjgl.opengl.GL11;


public class Utils
  extends Gui
{
  public static Utils INSTANCE;
  public static final int CLICK_INTERVAL = 250;
  public static final int INSTRUCTION_ADD_GOLEM_UPGRADE = 90;
  public static final int INSTRUCTION_REMOVE_FARMER_UPGRADE = 100;
  
  public Utils()
  {
    INSTANCE = this;
  }
  



  public void drawHoveringText(List<String> p_146283_1_, int p_146283_2_, int p_146283_3_, int screenWidth, int screenHeight, int maxTextWidth, FontRenderer font)
  {
    if (!p_146283_1_.isEmpty())
    {
      GL11.glDisable(32826);
      RenderHelper.disableStandardItemLighting();
      GL11.glDisable(2896);
      GL11.glDisable(2929);
      int k = 0;
      Iterator iterator = p_146283_1_.iterator();
      
      while (iterator.hasNext())
      {
        String s = (String)iterator.next();
        int l = font.getStringWidth(s);
        
        if (l > k)
        {
          k = l;
        }
      }
      
      int j2 = p_146283_2_ + 12;
      int k2 = p_146283_3_ - 12;
      int i1 = 8;
      
      if (p_146283_1_.size() > 1)
      {
        i1 += 2 + (p_146283_1_.size() - 1) * 10;
      }
      

      this.zLevel = 300.0F;
      int j1 = -267386864;
      drawGradientRect(j2 - 3, k2 - 4, j2 + k + 3, k2 - 3, j1, j1);
      drawGradientRect(j2 - 3, k2 + i1 + 3, j2 + k + 3, k2 + i1 + 4, j1, j1);
      drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 + i1 + 3, j1, j1);
      drawGradientRect(j2 - 4, k2 - 3, j2 - 3, k2 + i1 + 3, j1, j1);
      drawGradientRect(j2 + k + 3, k2 - 3, j2 + k + 4, k2 + i1 + 3, j1, j1);
      int k1 = 1347420415;
      int l1 = (k1 & 0xFEFEFE) >> 1 | k1 & 0xFF000000;
      drawGradientRect(j2 - 3, k2 - 3 + 1, j2 - 3 + 1, k2 + i1 + 3 - 1, k1, l1);
      drawGradientRect(j2 + k + 2, k2 - 3 + 1, j2 + k + 3, k2 + i1 + 3 - 1, k1, l1);
      drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 - 3 + 1, k1, k1);
      drawGradientRect(j2 - 3, k2 + i1 + 2, j2 + k + 3, k2 + i1 + 3, l1, l1);
      
      for (int i2 = 0; i2 < p_146283_1_.size(); i2++)
      {
        String s1 = (String)p_146283_1_.get(i2);
        font.drawStringWithShadow(s1, j2, k2, -1);
        
        if (i2 == 0)
        {
          k2 += 2;
        }
        
        k2 += 10;
      }
      
      this.zLevel = 0.0F;
      GL11.glEnable(2896);
      GL11.glEnable(2929);
      RenderHelper.enableStandardItemLighting();
      GL11.glEnable(32826);
    }
  }
}
