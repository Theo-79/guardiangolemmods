package fr.welsymc.guardiangolem.client.renderer;

import net.minecraft.util.IChatComponent;

public abstract interface IBossDisplayDataGuardian
{
  public abstract float getMaxHealth();
  
  public abstract float getHealth();
  
  public abstract IChatComponent func_145748_c_();
}
