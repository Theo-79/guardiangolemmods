package fr.welsymc.guardiangolem.client.renderer;

import fr.welsymc.guardiangolem.client.renderer.cosmetics.ICosmeticHatModel;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.items.cosmetics.hats.ICosmeticHat;
import fr.welsymc.guardiangolem.common.items.cosmetics.skins.ICosmeticSkin;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderGolem extends RenderLiving
{
  public final ResourceLocation texture = new ResourceLocation("guardiangolem", "textures/entity/golem.png");
  public final ResourceLocation kawaiTextureEaster = new ResourceLocation("guardiangolem", "textures/entity/kawaiGolem.png");
  
  public RenderGolem(ModelBase p_i1262_1_, float p_i1262_2_) {
    super(p_i1262_1_, p_i1262_2_);
  }
  
  protected ResourceLocation getEntityTexture(Entity e)
  {
    if ((e instanceof EntityGolem))
    {
      EntityGolem golem = (EntityGolem)e;
      
      if (golem.stackList[2] != null) {
        ItemStack item = golem.stackList[2];
        ICosmeticSkin cosmetic = (ICosmeticSkin)item.getItem();
        return cosmetic.texture();
      }
      
      if ((golem.getName() != null) && (golem.getName().trim().length() > 0))
      {
        if (((EntityGolem)e).getName().contains("Protector R.Y"))
        {
          return this.kawaiTextureEaster;
        }
      }
    }
    return this.texture;
  }
  
  public void doRender(EntityLiving e, double x, double y, double z, float entityYaw, float partialTicks) {
    if ((e instanceof EntityGolem)) {
      EntityGolem golem = (EntityGolem)e;
      
      if (golem.stackList[4] != null) {
        ItemStack item = golem.stackList[4];
        ICosmeticHat cosmetic = (ICosmeticHat)item.getItem();
        GL11.glPushMatrix();
        
        GL11.glTranslated(x, y + 2.0D, z);
        
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        
        GL11.glRotatef(e.renderYawOffset, 0.0F, 1.0F, 0.0F);
        
        GL11.glTranslated(0.0D, 0.0D, 0.22D);
        GL11.glRotatef(-e.renderYawOffset, 0.0F, 1.0F, 0.0F);
        
        GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        
        GL11.glRotatef(e.getRotationYawHead(), 0.0F, 1.0F, 0.0F);
        
        GL11.glRotatef(e.rotationPitch, 1.0F, 0.0F, 0.0F);
        
        bindTexture(cosmetic.texture());
        cosmetic.model().renderAll();
        GL11.glPopMatrix();
      }
    }
    super.doRender(e, x, y, z, entityYaw, partialTicks);
  }
}
