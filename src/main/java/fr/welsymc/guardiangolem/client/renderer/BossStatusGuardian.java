package fr.welsymc.guardiangolem.client.renderer;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;

public class BossStatusGuardian {
  public static float healthScale;
  public static int statusBarTime;
  public static String bossName;
  public static boolean hasColorModifier;
  public static EntityGolem guardian;
  
  public BossStatusGuardian() {}
  
  public static void setBossStatus(EntityGolem golem, boolean p_82824_1_) {
    if ((guardian != null) && 
      (guardian.getDistanceToEntity(net.minecraft.client.Minecraft.getMinecraft().thePlayer) < golem.getDistanceToEntity(net.minecraft.client.Minecraft.getMinecraft().thePlayer))) {
      return;
    }
    
    guardian = golem;
    healthScale = golem.getHealth() / golem.getMaxHealth();
    statusBarTime = 200;
    bossName = golem.getName();
    
    hasColorModifier = p_82824_1_;
  }
}
