package fr.welsymc.guardiangolem.client.renderer.cosmetics;

import net.minecraft.client.model.ModelRenderer;

public class ModelLeprechaunCosmetic extends ICosmeticHatModel
{
  private final ModelRenderer HAT;
  
  public ModelLeprechaunCosmetic()
  {
    this.textureWidth = 64;
    this.textureHeight = 32;
    
    this.HAT = new ModelRenderer(this);
    this.HAT.setRotationPoint(0.0F, -8.0F, 0.0F);
    this.HAT.cubeList.add(new net.minecraft.client.model.ModelBox(this.HAT, 0, 0, -8.0F, 0.0F, -8.0F, 16, 1, 16, 0.0F));
    this.HAT.cubeList.add(new net.minecraft.client.model.ModelBox(this.HAT, 0, 17, -4.5F, -4.0F, -4.5F, 9, 4, 9, 0.0F));
  }
  
  public void renderAll()
  {
    this.HAT.render(0.0625F);
  }
}
