package fr.welsymc.guardiangolem;

import com.sun.istack.internal.FragmentContentHandler;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import fr.welsymc.guardiangolem.client.Utils;
import fr.welsymc.guardiangolem.common.commands.CommandGuardianGolem;
import fr.welsymc.guardiangolem.common.entities.golemthings.GuardianKeeperHandler;
import fr.welsymc.guardiangolem.common.init.BlockInit;
import fr.welsymc.guardiangolem.common.init.EntityInit;
import fr.welsymc.guardiangolem.common.init.ItemInit;
import fr.welsymc.guardiangolem.common.network.CSOpenTree;
import fr.welsymc.guardiangolem.common.network.CSRenameGolemItem;
import fr.welsymc.guardiangolem.common.network.PacketSendInstructions;
import fr.welsymc.guardiangolem.common.network.SCGolemUpdate;
import fr.welsymc.guardiangolem.common.utils.GuiHandler;
import fr.welsymc.guardiangolem.proxy.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;

@Mod(modid="guardiangolem", name="Guardian Golem Reborn", version="0.1-INDEV")
public class GuardianGolem
{
  @cpw.mods.fml.common.Mod.Instance("guardiangolem")
  public static GuardianGolem instance;
  @cpw.mods.fml.common.SidedProxy(clientSide="fr.welsymc.guardiangolem.proxy.ClientProxy", serverSide="fr.welsymc.guardiangolem.proxy.CommonProxy")
  public static CommonProxy proxy;
  public static SimpleNetworkWrapper network;
  public static GuardianKeeperHandler keeperHandler;
  
  public GuardianGolem() {}
  
  @Mod.EventHandler
  public void preInit(FMLPreInitializationEvent e)
  {
    network = NetworkRegistry.INSTANCE.newSimpleChannel("guardiangolempacket");
    network.registerMessage(SCGolemUpdate.Handler.class, fr.welsymc.guardiangolem.common.network.SCGolemUpdate.class, 0, Side.CLIENT);
    network.registerMessage(CSOpenTree.Handler.class, CSOpenTree.class, 1, Side.SERVER);
    network.registerMessage(PacketSendInstructions.Handler.class, PacketSendInstructions.class, 2, Side.SERVER);
    network.registerMessage(fr.welsymc.guardiangolem.common.network.CSRenameGolemItem.Handler.class, CSRenameGolemItem.class, 3, Side.SERVER);
    
    EntityInit.init();
    ItemInit.init();
    BlockInit.init();
    proxy.registerRenderer();
    fr.welsymc.guardiangolem.common.items.guardianupgrades.ModGuardianUpgrades.init();
    NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
    MinecraftForge.EVENT_BUS.register(new fr.welsymc.guardiangolem.common.events.EventHandler());
    FMLCommonHandler.instance().bus().register(new fr.welsymc.guardiangolem.common.events.EventHandler());
    keeperHandler = new GuardianKeeperHandler();
  }
  
  public static final CreativeTabs TAB = new CreativeTabs("guardiangolem")
  {
    public Item getTabIconItem()
    {
      return ItemInit.GUARDIAN_STONE;
    }
  };
  
  @Mod.EventHandler
  public void init(FMLInitializationEvent e)
  {
    if (e.getSide() == Side.CLIENT) {
      new Utils();
    }
  }
  

  @Mod.EventHandler
  public void postInit(FMLPostInitializationEvent e) {}
  
  @Mod.EventHandler
  public void serverLoad(FMLServerStartingEvent event)
  {
    event.registerServerCommand(new fr.welsymc.guardiangolem.common.commands.CommandWhitelistGolem());
    event.registerServerCommand(new CommandGuardianGolem());
  }
}
