package fr.welsymc.guardiangolem.common.commands;

import fr.welsymc.guardiangolem.common.items.ItemWhitelist;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class CommandWhitelistGolem extends CommandBase
{
  public CommandWhitelistGolem() {}
  
  public String getCommandName()
  {
    return "whitelistgolem";
  }
  
  public String getCommandUsage(ICommandSender sender)
  {
    return "/" + getCommandName() + " add|remove <player>";
  }
  


  public void processCommand(ICommandSender sender, String[] args)
  {
    if (args.length < 2) {
      throw new WrongUsageException(getCommandUsage(sender), new Object[0]);
    }
    String mode = args[0];
    String player = args[1];
    if (!fr.welsymc.guardiangolem.common.utils.AntiOverloadVerifier.verify(player))
    {
      sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Bien essay� mais c'est r�t�..."));
      return;
    }
    
    if (mode.equals("add"))
    {
      NBTTagCompound compound = new NBTTagCompound();
      NBTTagList list = null;
      if ((((EntityPlayer)sender).inventory.getCurrentItem() != null) && ((((EntityPlayer)sender).inventory.getCurrentItem().getItem() instanceof ItemWhitelist)))
      {
        ItemStack hand = ((EntityPlayer)sender).inventory.getCurrentItem();
        if (!hand.hasTagCompound()) {
          hand.setTagCompound(new NBTTagCompound());
        }
        
        if (hand.getTagCompound().hasKey("players"))
        {
          list = hand.getTagCompound().getTagList("players", 8);
        }
        else {
          list = new NBTTagList();
        }
        if (list.tagCount() > 60) {
          sender.addChatMessage(new ChatComponentText("La limite de 60 joueurs a �t� atteinte !"));
          return;
        }
        boolean alreadyWhitelisted = false;
        
        for (int i = 0; i < list.tagCount(); i++)
        {
          if (list.getStringTagAt(i).toLowerCase().equals(player.toLowerCase()))
          {
            alreadyWhitelisted = true;
          }
        }
        if (alreadyWhitelisted)
        {
          sender.addChatMessage(new ChatComponentText("Hey, ce joueur est d�j� whitelist."));
          return;
        }
        if ((player != null) && (player.length() > 16)) {
          sender.addChatMessage(new ChatComponentText("Le pseudo ne doit pas d�passer 16 caract�res !"));
          return;
        }
        list.appendTag(new NBTTagString(player));
        compound.setTag("players", list);
        ((EntityPlayer)sender).inventory.getCurrentItem().setTagCompound(compound);
        sender.addChatMessage(new ChatComponentText("Vous venez d'ajouter " + EnumChatFormatting.RED + player + EnumChatFormatting.RESET + " � cette whitelist."));
      } else {
        sender.addChatMessage(new ChatComponentText("Vous devez avoir une Guardian Whitelist en main."));
      }
    } else if (mode.equals("remove"))
    {
      NBTTagCompound compound = new NBTTagCompound();
      NBTTagList list = null;
      ItemStack hand = ((EntityPlayer)sender).inventory.getCurrentItem();
      if (!hand.hasTagCompound()) {
        hand.setTagCompound(new NBTTagCompound());
      }
      
      if (hand.getTagCompound().hasKey("players"))
      {
        list = hand.getTagCompound().getTagList("players", 8);
      }
      else {
        list = new NBTTagList();
      }
      boolean alreadyWhitelisted = false;
      
      for (int i = 0; i < list.tagCount(); i++)
      {
        if (list.getStringTagAt(i).toLowerCase().equals(player.toLowerCase()))
        {
          alreadyWhitelisted = true;
          list.removeTag(i);
        }
      }
      if (alreadyWhitelisted)
      {
        sender.addChatMessage(new ChatComponentText("Vous venez d'enlever " + EnumChatFormatting.RED + player + EnumChatFormatting.RESET + " de cette whitelist."));
        compound.setTag("players", list);
        ((EntityPlayer)sender).inventory.getCurrentItem().setTagCompound(compound);
      } else {
        sender.addChatMessage(new ChatComponentText("Le joueur " + EnumChatFormatting.GOLD + player + EnumChatFormatting.RESET + " n'est pas whitelist."));
      }
    }
    else
    {
      throw new WrongUsageException(getCommandUsage(sender), new Object[0]);
    }
  }
}
