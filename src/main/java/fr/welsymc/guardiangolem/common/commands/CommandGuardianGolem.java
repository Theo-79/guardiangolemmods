package fr.welsymc.guardiangolem.common.commands;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.items.guardianupgrades.ModGuardianUpgrades;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class CommandGuardianGolem extends net.minecraft.command.CommandBase
{
  public CommandGuardianGolem() {}
  
  public String getCommandName()
  {
    return "guardiangolem";
  }
  


  public String getCommandUsage(ICommandSender sender) { return "/guardiangolem <owner> <level> <count> <upgrades_id...>"; }
  
  public void processCommand(ICommandSender sender, String[] args) {
    EntityPlayer player;
    String owner;
    if ((sender instanceof EntityPlayer)) {
      player = (EntityPlayer)sender;
      if (args.length >= 4) {
        owner = args[0];
        EntityPlayer target = player.worldObj.getPlayerEntityByName(owner);
        if (target == null) {
          player.addChatMessage(new ChatComponentText("�Ce joueur n'est pas en ligne"));
          return;
        }
        int level = Integer.valueOf(args[1]).intValue();
        int count = Integer.valueOf(args[2]).intValue();
        String[] upgrades = new String[args.length - 3];
        for (int i = 3; i < args.length; i++) {
          upgrades[(i - 3)] = args[i];
        }
        for (int i = 0; i < count; i++) {
          spawnGolem(player, target, level, upgrades);
        }
      } else if (args.length == 3) {
        owner = args[0];
        EntityPlayer target = player.worldObj.getPlayerEntityByName(owner);
        if (target == null) {
          player.addChatMessage(new ChatComponentText("�cCe joueur n'est pas en ligne"));
          return;
        }
        int level = Integer.valueOf(args[1]).intValue();
        int count = Integer.valueOf(args[2]).intValue();
        for (int i = 0; i < count; i++) {
          spawnGolem(player, target, level);
        }
      } else {
        player.addChatMessage(new ChatComponentText(getCommandUsage(sender)));
        player.addChatMessage(new ChatComponentText("�eListe des upgrades : "));
        for (int id : ModGuardianUpgrades.list.keySet()) {
            String name = ModGuardianUpgrades.get(id).getName();
            player.addChatMessage(new ChatComponentText("   �b[" + id + "] " + name));
        }
      }
    }
  }
  
  private void spawnGolem(EntityPlayer p, EntityPlayer owner, int level, String... upgrades) {
    World w = p.worldObj;
    int x = (int)p.posX;
    int y = (int)p.posY;
    int z = (int)p.posZ;
    EntityGolem golly = new EntityGolem(w);
    golly.setPosition(x, y, z);
    golly.setOwner(owner.getGameProfile().getId().toString());
    w.spawnEntityInWorld(golly);
    for (int i = 0; i < level; i++) {
      golly.addXP(fr.welsymc.guardiangolem.common.utils.GuardianUtils.getXPForLevel(i + 1));
    }
    if (upgrades != null) {
      for (String upgradeStr : upgrades) {
        int upgrade = Integer.valueOf(upgradeStr).intValue();
        if ((upgrade == 28) && 
          (!golly.hasUpgrade(28))) {
          golly.targetTasks.removeTask(golly.entityAIGuardian);
          golly.targetTasks.removeTask(golly.entityAISwimming);
          
          golly.targetTasks.removeTask(golly.entityAIMoveTowardsRestriction);
          golly.targetTasks.removeTask(golly.entityAIWander);
          golly.targetTasks.removeTask(golly.entityAIWatchClosest);
          golly.targetTasks.removeTask(golly.entityAILookIdle);
          golly.targetTasks.removeTask(golly.entityAIAttackPlayer);
          golly.targetTasks.removeTask(golly.entityAIAttackWither);
          golly.targetTasks.removeTask(golly.entityAIAttackGolem);
          
          golly.isFarmer = true;
        }
        golly.addUpgrade(upgrade);
        
        fr.welsymc.guardiangolem.GuardianGolem.network.sendToAllAround(new fr.welsymc.guardiangolem.common.network.SCGolemUpdate(golly.getEntityId(), golly.isFarmer, golly.getOwner(), golly.getXp(), golly.getLevel(), golly.getSubLevel(), golly.getCapacityPoints(), golly.getHealth(), golly.getEntityAttribute(SharedMonsterAttributes.maxHealth).getBaseValue(), golly.getEntityAttribute(SharedMonsterAttributes.attackDamage).getBaseValue(), golly.hasCustomName() ? golly.getName() : "Guardian Golem", golly.getUpgrades(), golly.getLastKnownOwnerName()), new cpw.mods.fml.common.network.NetworkRegistry.TargetPoint(p.worldObj.provider.dimensionId, p.posX, p.posY, p.posZ, 16.0D));
      }
    }
  }
}
