package fr.welsymc.guardiangolem.common.entities.golemthings;

import java.util.HashMap;
import java.util.Map;

import fr.welsymc.guardiangolem.common.inventories.InventoryGuardianKeeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;


public class GuardianKeeperHandler
  extends WorldSavedData
{
  HashMap<String, InventoryGuardianKeeper> guardianMap;
  
  public GuardianKeeperHandler()
  {
    super("guardiangolem_guardianKeeper");
    this.guardianMap = new HashMap();
  }
  
  public GuardianKeeperHandler(String s) {
    super(s);
    this.guardianMap = new HashMap();
  }
  
  public void readFromNBT(NBTTagCompound tag)
  {
    NBTTagList list = tag.getTagList("data", 10);
    for (int u = 0; u < list.tagCount(); u++) {
      NBTTagCompound entry = list.getCompoundTagAt(u);
      String uuid = entry.getString("uuid");
      InventoryGuardianKeeper inventory = new InventoryGuardianKeeper();
      NBTTagList nbttaglist = entry.getTagList("Items", 10);
      for (int i = 0; i < nbttaglist.tagCount(); i++) {
        NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
        int j = nbttagcompound1.getByte("Slot") & 0xFF;
        if ((j >= 0) && (j < inventory.getSizeInventory())) {
          inventory.setInventorySlotContents(i, ItemStack.loadItemStackFromNBT(nbttagcompound1));
        }
      }
      this.guardianMap.put(uuid, inventory);
    }
  }
  
  public void writeToNBT(NBTTagCompound tag)
  {
    NBTTagList list = new NBTTagList();
    for (Map.Entry<String, InventoryGuardianKeeper> entry : this.guardianMap.entrySet()) {
      NBTTagList nbttaglist = new NBTTagList();
      for (int i = 0; i < ((InventoryGuardianKeeper)entry.getValue()).getSizeInventory(); i++) {
        if (((InventoryGuardianKeeper)entry.getValue()).getStackInSlot(i) != null) {
          NBTTagCompound nbttagcompound1 = new NBTTagCompound();
          nbttagcompound1.setByte("Slot", (byte)i);
          ((InventoryGuardianKeeper)entry.getValue()).getStackInSlot(i).writeToNBT(nbttagcompound1);
          nbttaglist.appendTag(nbttagcompound1);
        }
      }
      NBTTagCompound tag2 = new NBTTagCompound();
      tag2.setString("uuid", (String)entry.getKey());
      tag2.setTag("Items", nbttaglist);
      list.appendTag(tag2);
    }
    
    tag.setTag("data", list);
  }
  
  public InventoryGuardianKeeper getStoneFromUUID(String uuid) {
    if (!this.guardianMap.containsKey(uuid))
      this.guardianMap.put(uuid, new InventoryGuardianKeeper());
    markDirty();
    return (InventoryGuardianKeeper)this.guardianMap.get(uuid);
  }
  
  public InventoryGuardianKeeper getStoneFromEntityPlayer(EntityPlayer entityPlayer) {
    if (this.guardianMap.containsKey(entityPlayer.getCommandSenderName())) {
      InventoryGuardianKeeper inventoryGuardianKeeperBASE = (InventoryGuardianKeeper)this.guardianMap.get(entityPlayer.getUniqueID().toString());
      for (int j = 0; j < inventoryGuardianKeeperBASE.getSizeInventory(); j++)
      {
        if (inventoryGuardianKeeperBASE.getStackInSlot(j) != null) {
          InventoryGuardianKeeper inv = getStoneFromUUID(entityPlayer.getUniqueID().toString());
          
          boolean found = false;
          for (int i = 0; i < inv.getSizeInventory(); i++) {
            if ((inv.getStackInSlot(i) == null) && (!found)) {
              inv.setInventorySlotContents(i, inventoryGuardianKeeperBASE.getStackInSlot(j));
              inventoryGuardianKeeperBASE.setInventorySlotContents(j, null);
              found = true;
            }
          }
        }
      }
      
      this.guardianMap.remove(entityPlayer.getCommandSenderName());
    }
    markDirty();
    return getStoneFromUUID(entityPlayer.getUniqueID().toString());
  }
  
  public static GuardianKeeperHandler get(World worldObj) {
    GuardianKeeperHandler handler = (GuardianKeeperHandler)worldObj.loadItemData(GuardianKeeperHandler.class, "guardiangolem_guardianKeeper");
    

    if (handler == null) {
      handler = new GuardianKeeperHandler();
      worldObj.setItemData("guardiangolem_guardianKeeper", handler);
    }
    
    return handler;
  }
}
