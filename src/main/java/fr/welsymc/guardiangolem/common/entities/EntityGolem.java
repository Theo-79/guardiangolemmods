package fr.welsymc.guardiangolem.common.entities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.ByteBufUtils;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.client.renderer.BossStatusGuardian;
import fr.welsymc.guardiangolem.common.ai.EntityAIGuardian;
import fr.welsymc.guardiangolem.common.entities.golemthings.GuardianKeeperHandler;
import fr.welsymc.guardiangolem.common.inventories.InventoryGuardianKeeper;
import fr.welsymc.guardiangolem.common.items.ItemGuardianHammer;
import fr.welsymc.guardiangolem.common.items.ItemGuardianStone;
import fr.welsymc.guardiangolem.common.items.cosmetics.particles.ICosmeticParticles;
import fr.welsymc.guardiangolem.common.items.guardianupgrades.GuardianUpgradeItem;
import fr.welsymc.guardiangolem.common.items.guardianupgrades.ModGuardianUpgrades;
import fr.welsymc.guardiangolem.common.network.SCGolemUpdate;
import fr.welsymc.guardiangolem.common.utils.GuardianUtils;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class EntityGolem extends EntityMob implements net.minecraft.inventory.ISidedInventory, fr.welsymc.guardiangolem.client.renderer.IBossDisplayDataGuardian, cpw.mods.fml.common.registry.IEntityAdditionalSpawnData
{
  private String owner = "";
  public ItemStack[] stackList;
  private int xp = 0;
  private int level;
  private int sub_level;
  private int capacitypoints;
  private int refreshTimer;
  private String customName = "";
  private String lastKnownOwnerName = "";
  
  HashMap<Integer, Integer> upgrades;
  
  public final EntityAIGuardian entityAIGuardian = new EntityAIGuardian(this, 1.0D, false);
  public final EntityAISwimming entityAISwimming = new EntityAISwimming(this);
  public final EntityAIMoveTowardsRestriction entityAIMoveTowardsRestriction = new EntityAIMoveTowardsRestriction(this, 1.0D);
  public final EntityAIWander entityAIWander = new EntityAIWander(this, 1.0D);
  public final EntityAIWatchClosest entityAIWatchClosest = new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F);
  public final EntityAILookIdle entityAILookIdle = new EntityAILookIdle(this);
  
  public final EntityAIBase entityAIAttackPlayer = new EntityAINearestAttackableTarget(this, EntityPlayer.class, 1, true);
  public final EntityAIBase entityAIAttackWither = new EntityAINearestAttackableTarget(this, net.minecraft.entity.boss.EntityWither.class, 1, true);
  public final EntityAIBase entityAIAttackGolem = new EntityAINearestAttackableTarget(this, EntityGolem.class, 1, true);
  
  public boolean isFarmer = false;
  
  public EntityGolem(World world) {
    super(world);
    this.stackList = new ItemStack[6];
    this.upgrades = new HashMap();
    this.tasks.taskEntries.clear();
    this.targetTasks.taskEntries.clear();
    
    if (!hasUpgrade(28)) {
      this.tasks.addTask(0, this.entityAIGuardian);
      this.tasks.addTask(1, this.entityAISwimming);
      
      this.tasks.addTask(5, this.entityAIMoveTowardsRestriction);
      this.tasks.addTask(7, this.entityAIWander);
      this.tasks.addTask(8, this.entityAIWatchClosest);
      this.tasks.addTask(8, this.entityAILookIdle);
      
      this.targetTasks.addTask(1, this.entityAIAttackPlayer);
      this.targetTasks.addTask(2, this.entityAIAttackWither);
      this.targetTasks.addTask(3, this.entityAIAttackGolem);
    } else {
      this.isFarmer = true;
    }
    setSize(1.5F, 2.75F);
  }
  


  public void applyEntityAttributes()
  {
    super.applyEntityAttributes();
    if (!this.isFarmer) {
      getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(20.0D);
      getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(6.0D);
      getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(0.5D);
      getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.23000000417232513D);
    } else {
      getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(1.0D);
      getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(1.0D);
      getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.0D);
      getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(0.0D);
      getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(0.0D);
    }
  }
  
  public HashMap<Integer, Integer> getUpgrades() {
    return this.upgrades;
  }
  
  public boolean isAIEnabled()
  {
    return !this.isFarmer;
  }
  
  public boolean canAttackWithItem()
  {
    return !this.isFarmer;
  }
  
  public void onDeath(DamageSource cause)
  {
    super.onDeath(cause);
    if ((cause.getEntity() != null) && ((cause.getEntity() instanceof EntityGolem))) {
      EntityGolem golem = (EntityGolem)cause.getEntity();
      if (golem.hasUpgrade(24)) {
        int subLevel = getSubLevel();
        int steal = this.worldObj.rand.nextInt(3);
        int xpToSteal = steal / 100 * subLevel;
        removeXP(xpToSteal);
        golem.addXP(xpToSteal);
      }
    }
    if ((getOwner() != null) && 
      (!this.worldObj.isRemote)) {
      InventoryGuardianKeeper inv = GuardianKeeperHandler.get(this.worldObj).getStoneFromUUID(getOwner());
      boolean found = false;
      for (int i = 0; i < inv.getSizeInventory(); i++) {
        if ((inv.getStackInSlot(i) == null) && (!found)) {
          inv.setInventorySlotContents(i, ItemGuardianStone.getGuardianStone(this));
          found = true;
        }
      }
    }
  }
  


  public void setLastKnownOwnerName(String lastKnownOwnerName)
  {
    this.lastKnownOwnerName = lastKnownOwnerName;
  }
  
  protected boolean interact(EntityPlayer p)
  {
    if ((this.owner.equals(p.getGameProfile().getId().toString())) && (!this.worldObj.isRemote)) {
      setLastKnownOwnerName(p.getCommandSenderName());
    }
    if (GuardianUtils.isGuardianFood(p.inventory.getCurrentItem()))
    {
      if (getLevel() >= 80) {
        return false;
      }
      for (int i1 = 0; i1 < 14; i1++)
      {
        double d0 = this.worldObj.rand.nextGaussian() * 0.02D;
        double d1 = this.worldObj.rand.nextGaussian() * 0.02D;
        double d2 = this.worldObj.rand.nextGaussian() * 0.02D;
        this.worldObj.spawnParticle("happyVillager", (float)this.posX + this.worldObj.rand.nextFloat() - this.worldObj.rand.nextFloat(), this.posY + 2.0D - this.worldObj.rand.nextGaussian() / 4.0D, (float)this.posZ + this.rand.nextGaussian() / 3.0D, d0, d1, d2);
      }
    }
    if (!this.worldObj.isRemote)
    {
      if (!this.isDead) {
        if (GuardianUtils.isGuardianFood(p.inventory.getCurrentItem()))
        {
          if (!p.isSneaking()) {
            int xpFromFood = GuardianUtils.getXPFromFood(p.inventory.getCurrentItem());
            int xpModifier = getUpgradeLevel(4) + 1;
            
            addXP(xpFromFood * xpModifier);
            if (!p.capabilities.isCreativeMode) {
              p.inventory.getCurrentItem().stackSize -= 1;
            }
          } else {
            int xpFromFood = GuardianUtils.getXPFromFood(p.inventory.getCurrentItem());
            int xpModifier = getUpgradeLevel(4) + 1;
            
            addXP(xpFromFood * xpModifier * p.inventory.getCurrentItem().stackSize);
            if (!p.capabilities.isCreativeMode) {
              p.setCurrentItemOrArmor(0, null);
            }
          }
        }
        else {
          if ((FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().func_152596_g(p.getGameProfile())) && 
            (p.inventory.getCurrentItem() != null) && (p.inventory.getCurrentItem().getItem() != null) && ((p.inventory.getCurrentItem().getItem() instanceof ItemGuardianHammer))) {
            attackEntityFrom(DamageSource.outOfWorld, Float.MAX_VALUE);
          }
          
          if (p.getGameProfile().getId().toString().equals(this.owner)) {
            setLastKnownOwnerName(p.getCommandSenderName());
            if ((p.inventory.getCurrentItem() != null) && (p.inventory.getCurrentItem().getItem() != null) && ((p.inventory.getCurrentItem().getItem() instanceof ItemGuardianHammer)))
            {
              attackEntityFrom(DamageSource.outOfWorld, Float.MAX_VALUE);
              
              p.inventory.getCurrentItem().damageItem(1, this);
            }
            else {
              GuardianGolem.network.sendTo(new SCGolemUpdate(getEntityId(), this.isFarmer, getOwner(), getXp(), 
                getLevel(), getSubLevel(), getCapacityPoints(), getHealth(), 
                getEntityAttribute(SharedMonsterAttributes.maxHealth).getBaseValue(), 
                getEntityAttribute(SharedMonsterAttributes.attackDamage).getBaseValue(), this.customName, this.upgrades, this.lastKnownOwnerName), (EntityPlayerMP)p);
              
              p.openGui(GuardianGolem.instance, 0, this.worldObj, getEntityId(), 0, 0);
            }
          } else {
            p.addChatMessage(new net.minecraft.util.ChatComponentText(EnumChatFormatting.DARK_RED + "Ce n'est pas votre golem !"));
          }
        }
      }
    }
    

    return super.interact(p);
  }
  
  public void removeXP(int xp) {
    if (getSubLevel() < xp) {
      setSubLevel(0);
    } else
      setSubLevel(getSubLevel() - xp);
  }
  
  public void addXP(int xp) {
    this.xp += xp;
    
    if (getLevel() >= 80) {
      setSubLevel(0);
      return;
    }
    
    setSubLevel(getSubLevel() + xp);
    if (getSubLevel() >= GuardianUtils.getXPForLevel(getLevel() + 1))
      levelUp();
  }
  
  public void addCapacityPoints(int points) {
    this.capacitypoints += points;
  }
  
  public void addCapacityPoint() {
    this.capacitypoints += 1;
  }
  
  public void levelUp() {
    if (getLevel() >= 80) {
      return;
    }
    setLevel(getLevel() + 1);
    int diff = getSubLevel() - GuardianUtils.getXPForLevel(getLevel());
    setSubLevel(0);
    addXP(diff);
    addCapacityPoints(5);
    
    List<EntityPlayer> players = this.worldObj.getEntitiesWithinAABB(EntityPlayer.class, 
      net.minecraft.util.AxisAlignedBB.getBoundingBox(this.posX - 32.0D, 0.0D, this.posZ - 32.0D, this.posX + 32.0D, 255.0D, this.posZ + 32.0D));
    
    for (EntityPlayer p : players) {
      if ((p instanceof EntityPlayerMP)) {
        GuardianGolem.network.sendTo(new SCGolemUpdate(getEntityId(), this.isFarmer, getOwner(), getXp(), 
          getLevel(), getSubLevel(), getCapacityPoints(), getHealth(), 
          getEntityAttribute(SharedMonsterAttributes.maxHealth).getBaseValue(), 
          getEntityAttribute(SharedMonsterAttributes.attackDamage).getBaseValue(), this.customName, 
          getUpgrades(), this.lastKnownOwnerName), (EntityPlayerMP)p);
      }
    }
  }
  
  public void addUpgradeLevel(int upgradeID) {
    GuardianUpgradeItem item = ModGuardianUpgrades.get(upgradeID);
    
    if (getCapacityPoints() < item.getCapacityPoints()) {
      return;
    }
    for (GuardianUpgradeItem parent : item.getParents()) {
      if (getUpgradeLevel(parent.getId()) <= 0)
        return;
    }
    this.upgrades.put(Integer.valueOf(upgradeID), Integer.valueOf(getUpgradeLevel(upgradeID) + 1));
  }
  
  public boolean hasUpgrade(int upgradeID) {
    return getUpgradeLevel(upgradeID) > 0;
  }
  
  public boolean addUpgrade(int upgradeId) {
    GuardianUpgradeItem upgradeItem = ModGuardianUpgrades.get(upgradeId);
    
    if (upgradeItem == null)
      return false;
    if (ModGuardianUpgrades.get(upgradeId).getMaxLevel() <= getUpgradeLevel(upgradeId))
      return false;
    if (getCapacityPoints() < ModGuardianUpgrades.get(upgradeId).getCapacityPoints()) {
      return false;
    }
    boolean flag = true;
    for (GuardianUpgradeItem item : ModGuardianUpgrades.get(upgradeId).getParents()) {
      flag = getUpgradeLevel(item.getId()) > 0;
    }
    if (!flag) {
      return false;
    }
    setCapacityPoints(getCapacityPoints() - ModGuardianUpgrades.get(upgradeId).getCapacityPoints());
    addUpgradeLevel(upgradeId);
    return true;
  }
  
  public int getCapacityPoints() {
    return this.capacitypoints;
  }
  
  public void setCapacityPoints(int capacity) {
    this.capacitypoints = capacity;
  }
  
  public int getUpgradeLevel(int id) {
    return ((Integer)getUpgrades().getOrDefault(Integer.valueOf(id), Integer.valueOf(0))).intValue();
  }
  
  public int getXp() {
    return this.xp;
  }
  
  public boolean hasCustomName()
  {
    return this.customName.trim().length() > 1;
  }
  
  public String getName() {
    return this.customName;
  }
  
  public void setCustomName(String s) {
    this.customName = s;
  }
  
  public int getSubLevel() {
    return this.sub_level;
  }
  
  public int getLevel() {
    return this.level;
  }
  
  public void setSubLevel(int sub) {
    this.sub_level = sub;
  }
  
  public void setLevel(int level) {
    this.level = level;
  }
  
  public int getTimer() {
    return this.refreshTimer;
  }
  
  public void setTimer(int tim) {
    this.refreshTimer = tim;
  }
  
  public void writeEntityToNBT(NBTTagCompound nbt)
  {
    super.writeEntityToNBT(nbt);
    nbt.setString("owner", this.owner);
    nbt.setInteger("xp", this.xp);
    nbt.setInteger("level", getLevel());
    nbt.setInteger("subLevel", getSubLevel());
    nbt.setInteger("capacitypoints", getCapacityPoints());
    nbt.setInteger("timer", getTimer());
    nbt.setString("customName", getName());
    NBTTagList itemList = new NBTTagList();
    ItemStack stack; for (int i = 0; i < this.stackList.length; i++) {
      stack = this.stackList[i];
      if (stack != null) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setByte("Slot", (byte)i);
        stack.writeToNBT(tag);
        itemList.appendTag(tag);
      }
    }
    nbt.setTag("GolemInventory", itemList);
    
    nbt.setInteger("listSize", this.upgrades.size());
    NBTTagList list = new NBTTagList();
    for (Map.Entry<Integer, Integer> e : this.upgrades.entrySet()) {
      NBTTagCompound compound = new NBTTagCompound();
      compound.setInteger("upgradeID", ((Integer)e.getKey()).intValue());
      compound.setInteger("upgradeLevel", ((Integer)e.getValue()).intValue());
      list.appendTag(compound);
    }
    nbt.setTag("upgradeList", list);
  }
  
  public void readEntityFromNBT(NBTTagCompound nbt)
  {
    super.readEntityFromNBT(nbt);
    this.owner = nbt.getString("owner");
    this.xp = nbt.getInteger("xp");
    setLevel(nbt.getInteger("level"));
    setSubLevel(nbt.getInteger("subLevel"));
    setCapacityPoints(nbt.getInteger("capacitypoints"));
    setTimer(nbt.getInteger("timer"));
    setCustomName(nbt.getString("customName"));
    int upgradeSize = nbt.getInteger("listSize");
    NBTTagList list = nbt.getTagList("upgradeList", 10);
    for (int i = 0; i < upgradeSize; i++) {
      NBTTagCompound tag = list.getCompoundTagAt(i);
      this.upgrades.put(Integer.valueOf(tag.getInteger("upgradeID")), Integer.valueOf(tag.getInteger("upgradeLevel")));
    }
    NBTTagList tagList = nbt.getTagList("GolemInventory", 10);
    this.stackList = new ItemStack[getSizeInventory()];
    for (int i = 0; i < tagList.tagCount(); i++) {
      NBTTagCompound tag = tagList.getCompoundTagAt(i);
      byte slot = tag.getByte("Slot");
      if ((slot >= 0) && (slot < this.stackList.length)) {
        this.stackList[slot] = ItemStack.loadItemStackFromNBT(tag);
      }
    }
    setLastKnownOwnerName(nbt.getString("lastOwner"));
    

    this.isFarmer = hasUpgrade(28);
  }
  
  public NBTTagCompound getCompound(NBTTagCompound nbt) {
    nbt.setString("owner", this.owner);
    nbt.setInteger("xp", this.xp);
    nbt.setInteger("level", getLevel());
    nbt.setInteger("subLevel", getSubLevel());
    nbt.setInteger("capacitypoints", getCapacityPoints());
    nbt.setInteger("timer", getTimer());
    
    NBTTagList itemList = new NBTTagList();
    ItemStack stack; for (int i = 0; i < this.stackList.length; i++) {
      stack = this.stackList[i];
      if (stack != null) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setByte("Slot", (byte)i);
        stack.writeToNBT(tag);
        itemList.appendTag(tag);
      }
    }
    nbt.setTag("GolemInventory", itemList);
    
    nbt.setInteger("listSize", this.upgrades.size());
    NBTTagList list = new NBTTagList();
    for (Map.Entry<Integer, Integer> e : this.upgrades.entrySet()) {
      NBTTagCompound compound = new NBTTagCompound();
      compound.setInteger("upgradeID", ((Integer)e.getKey()).intValue());
      compound.setInteger("upgradeLevel", ((Integer)e.getValue()).intValue());
      list.appendTag(compound);
    }
    nbt.setTag("upgradeList", list);
    nbt.setString("lastOwner", this.lastKnownOwnerName);
    return nbt;
  }
  

  public void onLivingUpdate()
  {
    if (!this.isFarmer) {
      if ((this.stackList[3] != null) && (this.worldObj.isRemote)) {
        ICosmeticParticles cosmetic = (ICosmeticParticles)this.stackList[3].getItem();
        cosmetic.update(this);
      }
      heal(0.25F);
      super.onLivingUpdate();
    }
    
    if (this.isFarmer) {
      if ((!hasUpgrade(26)) && (this.worldObj.isRemote) && (this.ticksExisted % 200 == 0)) {
        BossStatusGuardian.setBossStatus(this, true);
      }
      
      if (getUpgradeLevel(28) > 0) {
        ((GuardianUpgradeItem)ModGuardianUpgrades.list.get(Integer.valueOf(28))).updateGolem(getUpgradeLevel(28), this);
      }
    }
    else {
      boolean foundViolent = false;
      boolean foundHealth = false;
      getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(6.0D + getLevel());
      getEntityAttribute(SharedMonsterAttributes.maxHealth)
        .setBaseValue(100 + 100 * getLevel());
      getEntityAttribute(SharedMonsterAttributes.movementSpeed)
        .setBaseValue(0.23000000417232513D + 5.0E-15D * getLevel());
      
      for (Map.Entry<Integer, Integer> e : this.upgrades.entrySet()) {
        if (((Integer)e.getValue()).intValue() > 0) {
          ((GuardianUpgradeItem)ModGuardianUpgrades.list.get(e.getKey())).updateGolem(((Integer)e.getValue()).intValue(), this);
        }
      }
      heal(0.25F);
      setAlwaysRenderNameTag(false);
      if (!this.worldObj.isRemote)
      {
        if (getStackInSlot(1) != null) {
          if ((getStackInSlot(1).hasTagCompound()) && 
            (getStackInSlot(1).getTagCompound().getString("golemName").trim().length() > 0)) {
            setCustomName(getStackInSlot(1).getTagCompound().getString("golemName"));
            setCustomNameTag(getStackInSlot(1).getTagCompound().getString("golemName"));
          }
        } else {
          setCustomName("Guardian Golem");
          setCustomNameTag("Guardian Golem");
        }
      }
      
      if (!this.worldObj.isRemote) {
        if (this.refreshTimer < 50) {
          this.refreshTimer += 1;
        }
        
        if (this.refreshTimer >= 50)
        {









          this.refreshTimer = 0;
        }
      }
      

      if ((this.worldObj.isRemote) && (!hasUpgrade(26)) && (this.ticksExisted % 200 == 0)) {
        BossStatusGuardian.setBossStatus(this, true);
      }
    }
  }
  
  protected boolean canDespawn()
  {
    return false;
  }
  
  public int getSizeInventory()
  {
    return this.stackList.length;
  }
  
  public ItemStack getStackInSlot(int p_70301_1_)
  {
    return p_70301_1_ >= getSizeInventory() ? null : this.stackList[p_70301_1_];
  }
  
  public ItemStack decrStackSize(int p_70298_1_, int p_70298_2_)
  {
    if (this.stackList[p_70298_1_] != null)
    {

      if (this.stackList[p_70298_1_].stackSize <= p_70298_2_) {
        ItemStack itemstack = this.stackList[p_70298_1_];
        this.stackList[p_70298_1_] = null;
        return itemstack;
      }
      ItemStack itemstack = this.stackList[p_70298_1_].splitStack(p_70298_2_);
      
      if (this.stackList[p_70298_1_].stackSize == 0) {
        this.stackList[p_70298_1_] = null;
      }
      
      return itemstack;
    }
    
    return null;
  }
  

  public ItemStack getStackInSlotOnClosing(int p_70304_1_)
  {
    if (this.stackList[p_70304_1_] != null) {
      ItemStack itemstack = this.stackList[p_70304_1_];
      this.stackList[p_70304_1_] = null;
      return itemstack;
    }
    return null;
  }
  

  public void setInventorySlotContents(int p_70299_1_, ItemStack p_70299_2_)
  {
    this.stackList[p_70299_1_] = p_70299_2_;
  }
  
  public String getInventoryName()
  {
    return "palatrade.goleminventory";
  }
  
  public boolean hasCustomInventoryName()
  {
    return false;
  }
  
  public int getInventoryStackLimit()
  {
    return 1;
  }
  


  public void markDirty() {}
  

  public boolean isUseableByPlayer(EntityPlayer p_70300_1_)
  {
    return true;
  }
  


  public void openInventory() {}
  


  public void closeInventory() {}
  

  public String getLastKnownOwnerName()
  {
    return this.lastKnownOwnerName;
  }
  
  public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_)
  {
    return false;
  }
  
  public void setOwner(String name) {
    this.owner = name;
  }
  
  public String getOwner() {
    return this.owner;
  }
  
  public void setXp(int xp2) {
    this.xp = xp2;
  }
  
  public void setUpgrades(HashMap<Integer, Integer> map) {
    this.upgrades = map;
  }
  
  public int[] getAccessibleSlotsFromSide(int p_94128_1_)
  {
    return new int[0];
  }
  
  public boolean canInsertItem(int p_102007_1_, ItemStack p_102007_2_, int p_102007_3_)
  {
    return false;
  }
  
  public boolean canExtractItem(int p_102008_1_, ItemStack p_102008_2_, int p_102008_3_)
  {
    return false;
  }
  
  public void writeSpawnData(ByteBuf buffer)
  {
    ByteBufUtils.writeUTF8String(buffer, this.owner);
    buffer.writeInt(this.xp);
    buffer.writeInt(getLevel());
    buffer.writeInt(getSubLevel());
    buffer.writeInt(getCapacityPoints());
    buffer.writeInt(getTimer());
    ByteBufUtils.writeUTF8String(buffer, getName());
    
    buffer.writeInt(this.upgrades.size());
    for (Map.Entry<Integer, Integer> e : this.upgrades.entrySet()) {
      buffer.writeInt(((Integer)e.getKey()).intValue());
      buffer.writeInt(((Integer)e.getValue()).intValue());
    }
    
    buffer.writeInt(this.stackList.length);
    for (int i = 0; i < this.stackList.length; i++) {
      buffer.writeByte((byte)i);
      ItemStack stack = this.stackList[i];
      
      ByteBufUtils.writeItemStack(buffer, stack);
    }
    
    ByteBufUtils.writeUTF8String(buffer, getLastKnownOwnerName());
  }
  
  public void readSpawnData(ByteBuf additionalData)
  {
    this.owner = ByteBufUtils.readUTF8String(additionalData);
    this.xp = additionalData.readInt();
    setLevel(additionalData.readInt());
    setSubLevel(additionalData.readInt());
    setCapacityPoints(additionalData.readInt());
    setTimer(additionalData.readInt());
    setCustomName(ByteBufUtils.readUTF8String(additionalData));
    
    int upgradeSize = additionalData.readInt();
    for (int i = 0; i < upgradeSize; i++) {
      this.upgrades.put(Integer.valueOf(additionalData.readInt()), Integer.valueOf(additionalData.readInt()));
    }
    this.stackList = new ItemStack[getSizeInventory()];
    
    int size = additionalData.readInt();
    for (int i = 0; i < size; i++) {
      byte slot = additionalData.readByte();
      if ((slot >= 0) && (slot < this.stackList.length)) {
        this.stackList[slot] = ByteBufUtils.readItemStack(additionalData);
      }
    }
    setLastKnownOwnerName(ByteBufUtils.readUTF8String(additionalData));
    

    this.isFarmer = hasUpgrade(28);
  }
}
