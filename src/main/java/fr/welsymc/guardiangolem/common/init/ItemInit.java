package fr.welsymc.guardiangolem.common.init;

import fr.welsymc.guardiangolem.common.items.ItemCosmetic;
import fr.welsymc.guardiangolem.common.items.ItemGuardianHammer;
import fr.welsymc.guardiangolem.common.items.ItemGuardianStone;
import fr.welsymc.guardiangolem.common.items.ItemMiniGolem;
import fr.welsymc.guardiangolem.common.items.ItemWhitelist;
import fr.welsymc.guardiangolem.common.items.cosmetics.hats.ItemFarmerCosmetic;
import fr.welsymc.guardiangolem.common.items.cosmetics.hats.ItemLeprechaunCosmetic;
import fr.welsymc.guardiangolem.common.items.cosmetics.particles.ItemEnderCosmetic;
import fr.welsymc.guardiangolem.common.items.cosmetics.particles.ItemFlameCosmetic;
import fr.welsymc.guardiangolem.common.items.cosmetics.skins.ItemFuturistCosmetic;
import fr.welsymc.guardiangolem.common.items.cosmetics.skins.ItemSteampunkCosmetic;
import fr.welsymc.guardiangolem.common.utils.CosmeticType;
import net.minecraft.item.Item;

public class ItemInit
{
  public static final Item GUARDIAN_STONE = new ItemGuardianStone();
  public static final Item GUARDIAN_RENAMER = new fr.welsymc.guardiangolem.common.items.ItemGuardianRenamer();
  public static final Item GUARDIAN_WHITELIST = new ItemWhitelist();
  public static final Item GUARDIAN_HAMMER = new ItemGuardianHammer();
  
  public static final Item MINI_GOLEM = new ItemMiniGolem();
  



  public static final Item COSMETIC_HAT_FARMER = new ItemFarmerCosmetic("cosmetic_hat_farmer").setName("Chapeau de Farmer").setType(CosmeticType.HAT);
  public static final Item COSMETIC_HAT_LEPRECHAUN = new ItemLeprechaunCosmetic("cosmetic_hat_leprechaun").setName("Chapeau de Leprechaun").setType(CosmeticType.HAT);
  

  public static final Item COSMETIC_PARTICLES_FLAME = new ItemFlameCosmetic("cosmetic_particles_flame").setName("Particules de feu").setType(CosmeticType.PARTICLES);
  public static final Item COSMETIC_PARTICLES_ENDER = new ItemEnderCosmetic("cosmetic_particles_ender").setName("Particules de l'end").setType(CosmeticType.PARTICLES);
  

  public static final Item COSMETIC_SKIN_STEAMPUNK = new ItemSteampunkCosmetic("cosmetic_skin_steampunk").setName("Déguisement de Steampunk").setType(CosmeticType.SKIN);
  public static final Item COSMETIC_SKIN_FUTURIST = new ItemFuturistCosmetic("cosmetic_skin_futurist").setName("Déguisement de Futuriste").setType(CosmeticType.SKIN);
  
  private static final Item[] LIST = { GUARDIAN_STONE, GUARDIAN_RENAMER, GUARDIAN_WHITELIST, GUARDIAN_HAMMER, MINI_GOLEM, COSMETIC_HAT_FARMER, COSMETIC_HAT_LEPRECHAUN, COSMETIC_PARTICLES_FLAME, COSMETIC_PARTICLES_ENDER, COSMETIC_SKIN_STEAMPUNK, COSMETIC_SKIN_FUTURIST };
  








  public ItemInit() {}
  








  public static void init()
  {
    for (Item it : LIST)
    {
      cpw.mods.fml.common.registry.GameRegistry.registerItem(it, it.getUnlocalizedName().substring(5));
    }
  }
}
