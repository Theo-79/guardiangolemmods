package fr.welsymc.guardiangolem.common.init;

import cpw.mods.fml.common.registry.GameRegistry;
import fr.welsymc.guardiangolem.common.blocks.BlockGuardian;
import fr.welsymc.guardiangolem.common.blocks.BlockGuardianKeeper;
import net.minecraft.block.Block;

public class BlockInit
{
  public static final Block GUARDIAN_BLOCK = new BlockGuardian();
  public static final Block GUARDIAN_KEEPER = new BlockGuardianKeeper();
  
  public BlockInit() {}
  
  public static void init() { GameRegistry.registerBlock(GUARDIAN_BLOCK, GUARDIAN_BLOCK.getUnlocalizedName().substring(5));
    GameRegistry.registerBlock(GUARDIAN_KEEPER, GUARDIAN_KEEPER.getUnlocalizedName().substring(5));
  }
}
