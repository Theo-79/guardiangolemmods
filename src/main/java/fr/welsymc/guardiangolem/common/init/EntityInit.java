package fr.welsymc.guardiangolem.common.init;

import cpw.mods.fml.common.registry.EntityRegistry;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import java.awt.Color;

public class EntityInit
{
  public EntityInit() {}
  
  public static void init()
  {
    EntityRegistry.registerGlobalEntityID(EntityGolem.class, "guardianGolem", EntityRegistry.findGlobalUniqueEntityId(), new Color(0, 255, 0).getRGB(), new Color(255, 0, 0).getRGB());
    EntityRegistry.registerModEntity(EntityGolem.class, "guardianGolem", 420, GuardianGolem.instance, 40, 1, true);
  }
}
