package fr.welsymc.guardiangolem.common.utils;

public class LibUnifiedGui
{
  public static final int WIDTH_UNIFIED_GUI = 256;
  public static final int HEIGHT_UNIFIED_GUI = 166;
  public static final int WIDTH_SLIDE = 5;
  public static final int WIDTH_UNIFIED_BUTTON = 22;
  public static final int HEIGHT_UNIFIED_BUTTON = 22;
  public static final int X_BUTTON_OFFSET = 4;
  public static final int Y_BUTTON_OFFSET = 4;
  public static final int X_LIST_ELEMENT_OFFSET = 3;
  public static final int Y_LIST_ELEMENT_OFFSET = 3;
  public static final int BUTTON_LOGO_SIZE = 16;
  public static final int BUTTON_LOGO_Y_OFFSET = 5;
  public static final int BUTTON_LOGO_X_OFFSET = 5;
  public static final int DEFAULT_BUTTON = 0;
  public static final int WIDTH_ITEM_JOB = 58;
  public static final int HEIGHT_ITEM_JOB = 50;
  public static final int X_OFFSET_ITEM_JOB = 5;
  public static final int Y_OFFSET_ITEM_JOB = 10;
  public static final int JOBS_PER_ROW = 4;
  public static final int X_JOBS_OFFSET = -35;
  public static final int Y_JOBS_OFFSET = 40;
  public static final int Y_JOB_BAR_OFFSET = 32;
  public static final int WIDTH_LOGO_JOB = 20;
  public static final int HEIGHT_LOGO_JOB = 20;
  public static final int X_OFFSET_LOGO_JOB = 18;
  public static final int Y_OFFSET_LOGO_JOB = 1;
  public static final int Y_JOB_LEVEL_TEXT_OFFSET = 2;
  public static final int Y_JOB_NAME_TEXT_OFFSET = 2;
  public static final int Y_OFFSET_GUI_TOP = 8;
  public static final int X_JOB_HUD_OFFSET = 5;
  public static final int Y_JOB_HUD_OFFSET = 5;
  public static final int X_SHOP_OFFSET = 10;
  public static final int Y_SHOP_OFFSET = 10;
  public static final int X_XP_BAR_GUARDIAN_GOLEM = 6;
  public static final int Y_XP_BAR_GUARDIAN_GOLEM = 12;
  public static final int WIDTH_XP_BAR_GUARDIAN_GOLEM = 145;
  public static final int Y_OFFSET_BOSS_BAR = 20;
  public static final int WIDTH_BOSS_BAR = 200;
  public static final int X_OFFSET_BUTTON_SHOP = 5;
  public static final int Y_OFFSET_BUTTON_SHOP = 5;
  
  public LibUnifiedGui() {}
}
