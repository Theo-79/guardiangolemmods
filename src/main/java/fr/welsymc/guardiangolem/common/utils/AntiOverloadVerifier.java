package fr.welsymc.guardiangolem.common.utils;

public class AntiOverloadVerifier {
  public static final String REGEXALLOWED = "[A-Za-z�-��-��-�0-9\\s.;,_]*";
  
  public AntiOverloadVerifier() {}
  
  public static boolean verify(String s) {
    return s.matches("[A-Za-z�-��-��-�0-9\\s.;,_]*");
  }
}
