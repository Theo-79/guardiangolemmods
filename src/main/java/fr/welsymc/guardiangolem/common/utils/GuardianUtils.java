package fr.welsymc.guardiangolem.common.utils;

import com.mojang.authlib.GameProfile;
import fr.paladium.palamod.api.BlocksRegister;
import fr.paladium.palamod.api.ItemsRegister;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;



public class GuardianUtils
{
  public static final int LIFE_UPGRADE_ID = 0;
  public static final int BEACON_UPGRADE_ID = 1;
  public static final int GOLEMANCY_UPGRADE_ID = 2;
  public static final int VIOLENT_UPGRADE_ID = 3;
  public static final int GLUTON_UPGRADE_ID = 4;
  public static final int PENE_UPGARDE_ID = 5;
  public static final int THORNS_UPGARDE_ID = 6;
  public static final int RANGE_UPGRADE_ID = 7;
  public static final int WEAPON_UPGRADE_ID = 8;
  public static final int LIFESTEAL_UPGRADE_ID = 9;
  public static final int FREEZE_UPGRADE_ID = 10;
  public static final int BERZERK_UPGRADE_ID = 11;
  public static final int RESISTANCE_UPGRADE_ID = 12;
  public static final int SHIELD_UPGRADE_ID = 13;
  public static final int QUICK_UPGRADE_ID = 14;
  public static final int BUNNY_UPGRADE_ID = 15;
  public static final int WITHER_UPGRADE_ID = 16;
  public static final int ANCHOR_UPGRADE_ID = 17;
  public static final int BED_UPGRADE_ID = 18;
  public static final int WHISTLE_BLOWER_UPGRADE_ID = 19;
  public static final int EXPLODE_UPGRADE_ID = 20;
  public static final int STORAGE_UPGRADE_ID = 21;
  public static final int CLEAN_UPGRADE_ID = 22;
  public static final int BROTHER_KILLER_UPGRADE_ID = 23;
  public static final int CANIBAL_UPGRADE_ID = 24;
  public static final int HIDEN_UPGRADE_ID = 25;
  public static final int SECRET_UPGRADE_ID = 26;
  public static final int AUTOXP_UPGRADE_ID = 27;
  public static final int FARMER_UPGRADE_ID = 28;
  public static final double SIZE_ICON = 1.2D;
  public static final int X_OFFSET_ITEM_TREE = 5;
  public static final int Y_OFFSET_ITEM_TREE = 4;
  public static final int X_OFFSET_CAPACITY_POINTS_TREE = 3;
  public static final int Y_OFFSET_CAPACITY_POINTS_TREE = 13;
  public static final int CAPACITY_POINTS_PER_LEVEL = 1;
  public static final int DEFAULT_HEALTH = 100;
  public static final int HEALTH_PER_LEVEL = 100;
  public static final int AUTO_XP_TIME = 300;
  public static final int AUTO_XP_MAX = 10;
  public static final int FARMER_TIME = 300;
  public static final int MAX_LEVEL = 80;
  public static final int BEACON_RANGE = 15;
  public static final int EXPLOSION_RESIST_RANGE = 15;
  public static final int ATTACK_RANGE = 5;
  public static final float ATTACK_AMMOUNT = 1.0F;
  public static final String NAME_GUARDIAN_KEEPER_DATA = "guardiangolem_guardianKeeper";
  public static final int KEEPER_GUI = 2;
  public static final ResourceLocation GUARDIAN_KEEPER_GUI = new ResourceLocation("guardiangolem", "textures/gui/guardian_keeper.png");
  
  public GuardianUtils() {}
  
  public static void setEffectToGolem(final EntityGolem golem, int level, Potion effect, boolean inFaction) {
    AxisAlignedBB axisalignedbb = AxisAlignedBB.getBoundingBox(golem.posX, golem.posY, golem.posZ, golem.posX + 1.0D, golem.posY + 1.0D, golem.posZ + 1.0D).expand(15.0D, 15.0D, 15.0D).addCoord(0.0D, golem.worldObj.getHeight(), 0.0D);
    List<EntityPlayer> list = golem.worldObj.getEntitiesWithinAABB(EntityPlayer.class, axisalignedbb);
    






    List<Object> owners = (List)list.stream().filter(new Predicate()
    {
      public boolean test(EntityPlayer p) {
        return p.getGameProfile().getId().toString().equals(golem.getOwner());
      }

	@Override
	public boolean test(Object t) {
		// TODO Auto-generated method stub
		return false;
	}
    }).collect(Collectors.toList());
    
    for (Object entityplayerObj : owners) {
      EntityPlayer entityplayer = (EntityPlayer)entityplayerObj;
      if ((golem.getOwner() != null) && (entityplayer.getCommandSenderName().contentEquals(golem.getOwner())))
        entityplayer.addPotionEffect(new PotionEffect(effect.id, 120, level));
    }
    golem.addPotionEffect(new PotionEffect(effect.id, 120, level, true));
  }
  
  public static int getXPForLevel(int level) {
    return level * level * 4;
  }
  
  public static boolean isGuardianFood(ItemStack stack) {
    if ((stack == null) || (stack.getItem() == null)) {
      return false;
    }
    Item item = stack.getItem();
    
    if (item == ItemsRegister.PALADIUM_INGOT)
      return true;
    if (item == ItemsRegister.FINDIUM)
      return true;
    if (item == Item.getItemFromBlock(BlocksRegister.BLOCK_PALADIUM))
      return true;
    if (item == Item.getItemFromBlock(BlocksRegister.BLOCK_FINDIUM)) {
      return true;
    }
    return false;
  }
  
  public static int getXPFromFood(ItemStack stack) {
    if (!isGuardianFood(stack)) {
      return 0;
    }
    if ((stack == null) || (stack.getItem() == null)) {
      return 0;
    }
    Item item = stack.getItem();
    
    if (item == ItemsRegister.PALADIUM_INGOT)
      return 3;
    if (item == ItemsRegister.FINDIUM)
      return 6;
    if (item == Item.getItemFromBlock(BlocksRegister.BLOCK_PALADIUM))
      return 27;
    if (item == Item.getItemFromBlock(BlocksRegister.BLOCK_FINDIUM)) {
      return 54;
    }
    return 0;
  }
}
