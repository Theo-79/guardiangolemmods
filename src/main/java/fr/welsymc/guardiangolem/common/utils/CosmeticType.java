package fr.welsymc.guardiangolem.common.utils;

public enum CosmeticType
{
  HAT,  PARTICLES,  SKIN;
  
  private CosmeticType() {}
}
