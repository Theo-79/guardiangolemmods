package fr.welsymc.guardiangolem.common.utils;



public class LibRessources
{
  public static final String GUARDIAN_GOLEM = "guardiangolem:textures/entity/guardian_golem.png";
  
  public static final String SPECTRUM = "guardiangolem:textures/entity/spectrum.png";
  
  public static final String SPECTRUM_LAYER = "guardiangolem:textures/entity/spectrum_layer.png";
  
  public static String GUI_ELEMENTS = "guardiangolem:textures/gui/gui_elements.png";
  public static String GUI_UNIFIED_BAR = "guardiangolem:textures/gui/gui_bar.png";
  public static String GUI_UNIFIED_BAR_RAINBOW = "guardiangolem:textures/gui/gui_bar_rainbow.png";
  


  public static String ARMOR_COMPRESSOR_GUI = "guardiangolem:textures/gui/armorCompressor.png";
  public static String MAGICAL_ANVIL_GUI = "guardiangolem:textures/gui/magicalAnvil.png";
  public static String GUARDIAN_GOLEM_GUI = "guardiangolem:textures/gui/guardian_golem.png";
  public static String GUARDIAN_GOLEM_GUI_TREE = "guardiangolem:textures/gui/guardian_tree.png";
  public static String GUARDIAN_KEEPER_GUI = "guardiangolem:textures/gui/guardian_keeper.png";
  public static String INVENTORY_UNIFIED = "guardiangolem:textures/gui/inventory_unified.png";
  public static String LEVEL_UNIFIED = "guardiangolem:textures/gui/level_unified.png";
  public static String SPELL_UNIFIED = "guardiangolem:textures/gui/spell_unified.png";
  public static String FACTION_UNIFIED = "guardiangolem:textures/gui/spell_unified.png";
  public static String JOB_UNIFIED = "guardiangolem:textures/gui/jobs_unified.png";
  public static String COSMETIC_UNIFIED = "guardiangolem:textures/gui/spell_unified.png";
  public static String BACKGROUND_SHOP = "guardiangolem:textures/gui/background_shop.png";
  
  public static int[] INVENTORY_UNIFIED_ICON = { 64, 44 };
  public static int[] FACTION_UNIFIED_ICON = { 80, 44 };
  public static int[] LEVEL_UNIFIED_ICON = { 48, 44 };
  public static int[] SPELL_UNIFIED_ICON = { 16, 44 };
  public static int[] JOB_UNIFIED_ICON = { 0, 44 };
  public static int[] COSMETIC_UNIFIED_ICON = { 32, 44 };
  public static int[] DISABLED_SLOT = { 53, 0 };
  public static int[] OPEN_CHEST_ICON = { 97, 44 };
  public static int[] PALACOIN_ICON = { 0, 60 };
  


  public static String FISHERMAN_LOGO = "guardiangolem:textures/gui/job_icons.png";
  public static String HUNTER_LOGO = "guardiangolem:textures/gui/job_icons.png";
  public static String ASSASIN_LOGO = "guardiangolem:textures/gui/job_icons.png";
  public static String FARMER_LOGO = "guardiangolem:textures/gui/job_icons.png";
  public static String MINER_LOGO = "guardiangolem:textures/gui/job_icons.png";
  public static String ENCHANTER_LOGO = "guardiangolem:textures/gui/job_icons.png";
  public static String LUMBERJACK_LOGO = "guardiangolem:textures/gui/job_icons.png";
  public static String JOAILLIER_LOGO = "guardiangolem:textures/gui/job_icons.png";
  public static String ALCHIMISTE_LOGO = "guardiangolem:textures/gui/job_icons.png";
  
  public static int[] FISHERMAN_LOGO_COORDS = { 0, 60 };
  public static int[] HUNTER_LOGO_COORDS = { 0, 80 };
  public static int[] ASSASIN_LOGO_COORDS = { 0, 0 };
  public static int[] FARMER_LOGO_COORDS = { 0, 40 };
  public static int[] MINER_LOGO_COORDS = { 0, 140 };
  public static int[] ENCHANTER_LOGO_COORDS = { 0, 20 };
  public static int[] LUMBERJACK_LOGO_COORDS = { 0, 120 };
  public static int[] JOAILLIER_LOGO_COORDS = { 0, 100 };
  public static int[] ALCHIMISTE_LOGO_COORDS = { 0, 0 };
  


  public static String BATUM_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  public static String AERUM_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  public static String INERTIUM_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  public static String MENTALIS_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  public static String REPULSION_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  public static String EXPLODE_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  public static String PERCEPTION_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  public static String OMNISCIENCE_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  public static String OBSERVATUS_LOGO = "guardiangolem:textures/gui/spell_icons.png";
  
  public static int[] BATUM_LOGO_COORDS = { 196, 0 };
  public static int[] AERUM_LOGO_COORDS = { 224, 0 };
  public static int[] INERTIUM_LOGO_COORDS = { 168, 0 };
  public static int[] MENTALIS_LOGO_COORDS = { 140, 0 };
  public static int[] REPULSION_LOGO_COORDS = { 28, 0 };
  public static int[] EXPLODE_LOGO_COORDS = { 0, 0 };
  public static int[] PERCEPTION_LOGO_COORDS = { 56, 0 };
  public static int[] OMNISCIENCE_LOGO_COORDS = { 84, 0 };
  public static int[] OBSERVATUS_LOGO_COORDS = { 112, 0 };
  
  public static String BATUM_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  public static String AERUM_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  public static String INERTIUM_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  public static String MENTALIS_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  public static String REPULSION_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  public static String EXPLODE_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  public static String PERCEPTION_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  public static String OMNISCIENCE_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  public static String OBSERVATUS_IMAGE = "guardiangolem:textures/gui/spell_images.png";
  
  public static int[] BATUM_IMAGE_COORDS = { 0, 0 };
  public static int[] AERUM_IMAGE_COORDS = { 97, 100 };
  public static int[] INERTIUM_IMAGE_COORDS = { 97, 50 };
  public static int[] MENTALIS_IMAGE_COORDS = { 97, 0 };
  public static int[] REPULSION_IMAGE_COORDS = { 0, 150 };
  public static int[] EXPLODE_IMAGE_COORDS = { 0, 102 };
  public static int[] PERCEPTION_IMAGE_COORDS = { 0, 200 };
  public static int[] OMNISCIENCE_IMAGE_COORDS = { 0, 50 };
  public static int[] OBSERVATUS_IMAGE_COORDS = { 97, 150 };
  
  public static int SPELL_LOGO_SIZE = 28;
  


  public static int X_UNIFIED_BUTTON_1 = 0;
  public static int Y_UNIFIED_BUTTON_1 = 0;
  public static int UNIFIED_BAR_MAGENTA = 0;
  public static int UNIFIED_BAR_GREEN = 9;
  public static int UNIFIED_BAR_GREY = 18;
  public static int UNIFIED_BAR_RED = 27;
  
  public static int[] BUTTON_PLUS_COORDS = { 44, 0 };
  public static int[] BUTTON_DIGIT_COORDS = { 44, 18 };
  
  public static int X_OFFSET_SHOP_DISPLAYER = 5;
  


  public static int HEIGHT_UNIFIED_BAR = 9;
  public static int WIDTH_BUTTON_PLUS = 9;
  public static int WIDTH_BUTTON_DIGIT = 9;
  public static int WIDTH_GUARDIAN_GOLEM_GUI = 238;
  public static int HEIGHT_GUARDUAN_GOLEM_GUI = 227;
  public static int WIDTH_GUARDIAN_KEEPER_GUI = 175;
  public static int HEIGHT_GUARDUAN_KEEPER_GUI = 120;
  public static int WIDTH_BACKGROUND_SHOP = 512;
  public static int HEIGHT_BACKGROUND_SHOP = 512;
  public static int WIDTH_ICON_BUTTON_SHOP = 16;
  public static int HEIGHT_ICON_BUTTON_SHOP = 16;
  public static int WIDTH_SHOP_ENTRY = 300;
  public static int HEIGHT_SHOP_ENTRY = 50;
  public static int WIDTH_ICON_SHOP = 30;
  public static int HEIGHT_ICON_SHOP = 30;
  public static int SHOP_LIST_WIDTH = 300;
  
  public LibRessources() {}
}
