package fr.welsymc.guardiangolem.common.utils;

import cpw.mods.fml.common.network.IGuiHandler;
import fr.welsymc.guardiangolem.client.gui.GuiGuardianKeeper;
import fr.welsymc.guardiangolem.common.containers.ContainerGolem;
import fr.welsymc.guardiangolem.common.containers.ContainerGuardianKeeper;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler
{
  public GuiHandler() {}
  
  public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
  {
    switch (ID) {
    case 0: 
      if ((world.getEntityByID(x) == null) || (!(world.getEntityByID(x) instanceof EntityGolem)))
        return null;
      return new ContainerGolem(player, (EntityGolem)world.getEntityByID(x), false);
    case 1: 
      if ((world.getEntityByID(x) == null) || (!(world.getEntityByID(x) instanceof EntityGolem)))
        return null;
      return new ContainerGolem(player, (EntityGolem)world.getEntityByID(x), true);
    case 2: 
      return new ContainerGuardianKeeper(player);
    }
    
    
    return null;
  }
  
  public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
  {
    switch (ID) {
    case 0: 
      if ((world.getEntityByID(x) == null) || (!(world.getEntityByID(x) instanceof EntityGolem)))
        return null;
      return new fr.welsymc.guardiangolem.client.gui.GuiContainerGolem(new ContainerGolem(player, (EntityGolem)world.getEntityByID(x), false), (EntityGolem)world.getEntityByID(x));
    case 1: 
      if ((world.getEntityByID(x) == null) || (!(world.getEntityByID(x) instanceof EntityGolem)))
        return null;
      return new fr.welsymc.guardiangolem.client.gui.GuiGuardianGolemTree(new ContainerGolem(player, 
        (EntityGolem)world.getEntityByID(x), true));
    case 2: 
      return new GuiGuardianKeeper(new ContainerGuardianKeeper(player));
    }
    
    return null;
  }
}
