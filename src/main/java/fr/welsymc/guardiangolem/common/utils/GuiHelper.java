package fr.welsymc.guardiangolem.common.utils;

import org.lwjgl.opengl.GL11;

public class GuiHelper
{
  public GuiHelper() {}
  
  public static void drawLine(int x1, int y1, int x2, int y2, float r, float g, float b) {
    GL11.glPushMatrix();
    GL11.glTranslated(0.0D, 0.0D, 1.0D);
    net.minecraft.client.renderer.Tessellator tessellator = net.minecraft.client.renderer.Tessellator.instance;
    GL11.glDisable(2884);
    GL11.glDisable(2896);
    GL11.glDisable(3553);
    
    GL11.glEnable(3042);
    GL11.glBlendFunc(770, 771);
    GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    GL11.glLineWidth(2.0F);
    GL11.glDepthMask(false);
    
    tessellator.startDrawing(2);
    tessellator.setColorRGBA_F(r, g, b, 1.0F);
    tessellator.addVertex(x1, y1, 0.0D);
    tessellator.addVertex(x2, y2, 0.0D);
    
    tessellator.draw();
    GL11.glEnable(3553);
    GL11.glPopMatrix();
  }
}
