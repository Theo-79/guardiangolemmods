package fr.welsymc.guardiangolem.common.utils;

public class References
{
  public static final String MODID = "guardiangolem";
  public static final String VERSION = "0.1-INDEV";
  public static final String NAME = "Guardian Golem Reborn";
  public static final int LIFE_UPGRADE_ID = 0;
  public static final int BEACON_UPGRADE_ID = 1;
  public static final int GOLEMANCY_UPGRADE_ID = 2;
  public static final int VIOLENT_UPGRADE_ID = 3;
  public static final int GLUTON_UPGRADE_ID = 4;
  public static final int PENE_UPGARDE_ID = 5;
  public static final int THORNS_UPGARDE_ID = 6;
  public static final int RANGE_UPGRADE_ID = 7;
  public static final int WEAPON_UPGRADE_ID = 8;
  public static final int LIFESTEAL_UPGRADE_ID = 9;
  public static final int FREEZE_UPGRADE_ID = 10;
  public static final int BERZERK_UPGRADE_ID = 11;
  public static final int RESISTANCE_UPGRADE_ID = 12;
  public static final int SHIELD_UPGRADE_ID = 13;
  public static final int QUICK_UPGRADE_ID = 14;
  public static final int BUNNY_UPGRADE_ID = 15;
  public static final int WITHER_UPGRADE_ID = 16;
  public static final int ANCHOR_UPGRADE_ID = 17;
  public static final int BED_UPGRADE_ID = 18;
  public static final int WHISTLE_BLOWER_UPGRADE_ID = 19;
  public static final int EXPLODE_UPGRADE_ID = 20;
  public static final int STORAGE_UPGRADE_ID = 21;
  public static final int CLEAN_UPGRADE_ID = 22;
  public static final int BROTHER_KILLER_UPGRADE_ID = 23;
  public static final int CANIBAL_UPGRADE_ID = 24;
  public static final int HIDEN_UPGRADE_ID = 25;
  public static final int SECRET_UPGRADE_ID = 26;
  public static final int AUTOXP_UPGRADE_ID = 27;
  public static final int FARMER_UPGRADE_ID = 28;
  public static final double SIZE_ICON = 1.2D;
  public static final int X_OFFSET_ITEM_TREE = 5;
  public static final int Y_OFFSET_ITEM_TREE = 4;
  public static final int X_OFFSET_CAPACITY_POINTS_TREE = 3;
  public static final int Y_OFFSET_CAPACITY_POINTS_TREE = 13;
  public static final int CAPACITY_POINTS_PER_LEVEL = 1;
  public static final int DEFAULT_HEALTH = 100;
  public static final int HEALTH_PER_LEVEL = 100;
  public static final int AUTO_XP_TIME = 35;
  public static final int AUTO_XP_MAX = 10;
  public static final int FARMER_TIME = 1800;
  public static final int MAX_LEVEL = 80;
  public static final int BEACON_RANGE = 15;
  public static final int EXPLOSION_RESIST_RANGE = 15;
  public static final int ATTACK_RANGE = 3;
  public static final float ATTACK_AMMOUNT = 1.0F;
  
  public References() {}
}
