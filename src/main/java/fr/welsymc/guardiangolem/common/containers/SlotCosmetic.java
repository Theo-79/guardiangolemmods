package fr.welsymc.guardiangolem.common.containers;

import fr.welsymc.guardiangolem.common.items.ItemCosmetic;
import fr.welsymc.guardiangolem.common.utils.CosmeticType;
import net.minecraft.item.ItemStack;

public class SlotCosmetic extends net.minecraft.inventory.Slot
{
  private CosmeticType type;
  
  public SlotCosmetic(net.minecraft.inventory.IInventory inv, int index, int x, int y, CosmeticType type)
  {
    super(inv, index, x, y);
    this.type = type;
  }
  
  public boolean isItemValid(ItemStack item)
  {
    if ((item != null) && 
      ((item.getItem() instanceof ItemCosmetic))) {
      ItemCosmetic itemCosmetic = (ItemCosmetic)item.getItem();
      if (itemCosmetic.getType() == this.type) {
        return true;
      }
    }
    
    return false;
  }
}
