package fr.welsymc.guardiangolem.common.containers;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.utils.CosmeticType;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ContainerGolem extends Container
{
  EntityPlayer player;
  EntityGolem golem;
  boolean tree;
  
  public ContainerGolem(EntityPlayer player, EntityGolem golem, boolean tree)
  {
    this.player = player;
    this.golem = golem;
    this.tree = tree;
    
    if (!tree) {
      addSlotToContainer(new SlotWhitelist(golem, 0, 102, 52));
      addSlotToContainer(new SlotRename(golem, 1, 206, 52));
      
      addSlotToContainer(new SlotCosmetic(golem, 2, 84, 89, CosmeticType.SKIN));
      addSlotToContainer(new SlotCosmetic(golem, 3, 102, 89, CosmeticType.PARTICLES));
      addSlotToContainer(new SlotCosmetic(golem, 4, 120, 89, CosmeticType.HAT));
      
      bindPlayerInventory(player.inventory);
    } else if (!player.worldObj.isRemote) {
      loadDataAndSendToClient(player.getUniqueID(), player);
    }
  }
  
  private void loadDataAndSendToClient(UUID uniqueID, EntityPlayer player) { loadDataAndSendToClient(uniqueID, player, this.golem); }
  


  public static void loadDataAndSendToClient(UUID uniqueID, EntityPlayer player, EntityGolem golem) {}
  

  public void bindPlayerInventory(InventoryPlayer playerInventory)
  {
    for (int l = 0; l < 3; l++) {
      for (int j1 = 0; j1 < 9; j1++) {
        addSlotToContainer(new Slot(playerInventory, j1 + (l + 1) * 9, 39 + j1 * 18, 145 + l * 18));
      }
    }
    

    for (int i1 = 0; i1 < 9; i1++) {
      addSlotToContainer(new Slot(playerInventory, i1, 39 + i1 * 18, 203));
    }
  }
  

  public boolean canInteractWith(EntityPlayer playerIn)
  {
    return playerIn.getDistance(this.golem.posX, this.golem.posY, this.golem.posZ) <= 10.0D;
  }
  
  public EntityGolem getGolem() {
    return this.golem;
  }
  


  public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex)
  {
    return null;
  }
}
