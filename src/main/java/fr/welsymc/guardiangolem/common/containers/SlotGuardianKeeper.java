package fr.welsymc.guardiangolem.common.containers;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotGuardianKeeper extends Slot
{
  public SlotGuardianKeeper(IInventory inventoryIn, int index, int xPosition, int yPosition)
  {
    super(inventoryIn, index, xPosition, yPosition);
  }
  
  public boolean isItemValid(ItemStack stack)
  {
    if ((stack.getItem() instanceof fr.welsymc.guardiangolem.common.items.ItemGuardianStone))
      return true;
    return false;
  }
}
