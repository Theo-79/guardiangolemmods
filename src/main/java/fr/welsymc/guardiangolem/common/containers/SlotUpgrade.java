package fr.welsymc.guardiangolem.common.containers;

import fr.welsymc.guardiangolem.common.items.guardianupgrades.GuardianUpgradeItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotUpgrade extends Slot
{
  public SlotUpgrade(IInventory p_i1824_1_, int p_i1824_2_, int p_i1824_3_, int p_i1824_4_)
  {
    super(p_i1824_1_, p_i1824_2_, p_i1824_3_, p_i1824_4_);
  }
  
  public boolean isItemValid(ItemStack stack)
  {
    for (GuardianUpgradeItem it : fr.welsymc.guardiangolem.common.items.guardianupgrades.ModGuardianUpgrades.list.values())
    {
      if (it.getItemStack(0).isItemEqual(stack))
      {
        return true;
      }
    }
    return false;
  }
}
