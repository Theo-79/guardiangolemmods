package fr.welsymc.guardiangolem.common.containers;

import fr.welsymc.guardiangolem.common.items.ItemWhitelist;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotWhitelist extends Slot
{
  public SlotWhitelist(IInventory p_i1824_1_, int p_i1824_2_, int p_i1824_3_, int p_i1824_4_)
  {
    super(p_i1824_1_, p_i1824_2_, p_i1824_3_, p_i1824_4_);
  }
  
  public boolean isItemValid(ItemStack s)
  {
    return (s != null) && ((s.getItem() instanceof ItemWhitelist));
  }
}
