package fr.welsymc.guardiangolem.common.containers;

import fr.welsymc.guardiangolem.common.entities.golemthings.GuardianKeeperHandler;
import fr.welsymc.guardiangolem.common.inventories.InventoryGuardianKeeper;
import javax.annotation.Nullable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerGuardianKeeper extends Container
{
  EntityPlayer player;
  
  public ContainerGuardianKeeper(EntityPlayer player)
  {
    this.player = player;
    
    InventoryGuardianKeeper inventory = GuardianKeeperHandler.get(player.worldObj).getStoneFromEntityPlayer(player);
    
    for (int i = 0; i < 9; i++) {
      addSlotToContainer(new SlotGuardianKeeper(inventory, i, 8 + i * 18, 40));
    }
    
    bindPlayerInventory(player.inventory);
  }
  
  public void bindPlayerInventory(InventoryPlayer playerInventory) {
    for (int l = 0; l < 3; l++) {
      for (int j1 = 0; j1 < 9; j1++) {
        addSlotToContainer(new Slot(playerInventory, j1 + (l + 1) * 9, 8 + j1 * 18, 61 + l * 18));
      }
    }
    
    for (int i1 = 0; i1 < 9; i1++) {
      addSlotToContainer(new Slot(playerInventory, i1, 8 + i1 * 18, 119));
    }
  }
  
  public boolean canInteractWith(EntityPlayer playerIn)
  {
    return true;
  }
  
  @Nullable
  public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
  {
    ItemStack itemstack = null;
    Slot slot = (Slot)this.inventorySlots.get(index);
    
    return itemstack;
  }
}
