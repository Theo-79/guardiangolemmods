package fr.welsymc.guardiangolem.common.network;

import java.util.HashMap;
import java.util.Map;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;

public class SCGolemUpdate implements IMessage
{
  private int entity;
  private boolean isFarmer;
  private String owner;
  private int xp;
  private int level;
  private int sub_level;
  private int capacity;
  private double health;
  private double maxHealth;
  private double attackDamage;
  private String customName;
  private HashMap<Integer, Integer> map;
  private String lastKnowOwner;
  
  public SCGolemUpdate() {}
  
  public SCGolemUpdate(int entity, boolean isFarmer, String owner, int xp, int level, int sub_level, int capacity, double health, double maxHealth, double attackDamage, String customName, HashMap<Integer, Integer> upgrades, String lastKnowOwner)
  {
    this.entity = entity;
    this.isFarmer = isFarmer;
    this.owner = owner;
    this.xp = xp;
    this.level = level;
    this.sub_level = sub_level;
    this.capacity = capacity;
    this.health = health;
    this.maxHealth = maxHealth;
    this.attackDamage = attackDamage;
    this.customName = customName;
    this.map = upgrades;
    this.lastKnowOwner = lastKnowOwner;
  }
  
  public void fromBytes(ByteBuf buf)
  {
    this.entity = buf.readInt();
    this.isFarmer = buf.readBoolean();
    this.owner = ByteBufUtils.readUTF8String(buf);
    this.xp = buf.readInt();
    this.level = buf.readInt();
    this.sub_level = buf.readInt();
    this.capacity = buf.readInt();
    this.health = buf.readDouble();
    this.maxHealth = buf.readDouble();
    this.attackDamage = buf.readDouble();
    this.customName = ByteBufUtils.readUTF8String(buf);
    
    int size = buf.readInt();
    this.map = new HashMap(size);
    for (int i = 0; i < size; i++) {
      this.map.put(Integer.valueOf(buf.readInt()), Integer.valueOf(buf.readInt()));
    }
    
    this.lastKnowOwner = ByteBufUtils.readUTF8String(buf);
  }
  
  public void toBytes(ByteBuf buf)
  {
    buf.writeInt(this.entity);
    buf.writeBoolean(this.isFarmer);
    ByteBufUtils.writeUTF8String(buf, this.owner);
    buf.writeInt(this.xp);
    buf.writeInt(this.level);
    buf.writeInt(this.sub_level);
    buf.writeInt(this.capacity);
    buf.writeDouble(this.health);
    buf.writeDouble(this.maxHealth);
    buf.writeDouble(this.attackDamage);
    ByteBufUtils.writeUTF8String(buf, this.customName);
    buf.writeInt(this.map.size());
    for (Map.Entry<Integer, Integer> e : this.map.entrySet()) {
      int id = ((Integer)e.getKey()).intValue();
      int level = ((Integer)e.getValue()).intValue();
      buf.writeInt(id);
      buf.writeInt(level);
    }
    
    ByteBufUtils.writeUTF8String(buf, this.lastKnowOwner);
  }
  
  public static class Handler implements IMessageHandler<SCGolemUpdate, IMessage>
  {
    public Handler() {}
    
    public IMessage onMessage(SCGolemUpdate message, MessageContext ctx) {
      Entity golemEntity = Minecraft.getMinecraft().theWorld.getEntityByID(message.entity);
      
      if ((golemEntity != null) && ((golemEntity instanceof EntityGolem)))
      {
        EntityGolem golem = (EntityGolem)golemEntity;
        golem.isFarmer = message.isFarmer;
        golem.setOwner(message.owner);
        golem.setXp(message.xp);
        golem.setLevel(message.level);
        golem.setSubLevel(message.sub_level);
        golem.setCapacityPoints(message.capacity);
        
        golem.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(message.maxHealth);
        
        golem.setHealth((float)message.health);
        
        golem.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(message.attackDamage);
        
        golem.setCustomName(message.customName);
        golem.setUpgrades(message.map);
        
        golem.setLastKnownOwnerName(message.lastKnowOwner);
      }
      

      return null;
    }
  }
}
