package fr.welsymc.guardiangolem.common.network;

import com.mojang.authlib.GameProfile;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;

public class PacketSendInstructions implements IMessage
{
  int instruction;
  String[] params;
  
  public PacketSendInstructions() {}
  
  public PacketSendInstructions(int instruction, String[] params)
  {
    this.instruction = instruction;
    this.params = params;
  }
  
  public void fromBytes(ByteBuf buf)
  {
    this.instruction = buf.readInt();
    int arrayL = buf.readInt();
    if (arrayL == 0)
      return;
    this.params = new String[arrayL];
    for (int i = 0; i < arrayL; i++) {
      this.params[i] = ByteBufUtils.readUTF8String(buf);
    }
  }
  
  public void toBytes(ByteBuf buf)
  {
    buf.writeInt(this.instruction);
    buf.writeInt(this.params.length);
    for (String s : this.params) {
      ByteBufUtils.writeUTF8String(buf, s);
    }
  }
  
  public class Handler implements cpw.mods.fml.common.network.simpleimpl.IMessageHandler<PacketSendInstructions, IMessage> {
		    public IMessage onMessage(PacketSendInstructions message, MessageContext ctx) {
		        EntityPlayerMP player = ctx.getServerHandler().playerEntity;
		        if (message.instruction == 90) {
		            int golemId;
		            int upgradeId;
		            if (message.params.length < 2) {
		                return null;
		            }
		            try {
		                upgradeId = Integer.parseInt(message.params[0]);
		            }
		            catch (NumberFormatException e) {
		                return null;
		            }
		            try {
		                golemId = Integer.parseInt(message.params[1]);
		            }
		            catch (NumberFormatException e) {
		                return null;
		            }
		            Entity golemEntity = player.worldObj.getEntityByID(golemId);
		            if (golemEntity != null && golemEntity instanceof EntityGolem && ((EntityGolem)golemEntity).getOwner().equals(ctx.getServerHandler().playerEntity.getGameProfile().getId().toString())) {
		                EntityGolem golem = (EntityGolem)golemEntity;
		                if (upgradeId == 28 && !golem.hasUpgrade(28)) {
		                    golem.targetTasks.removeTask(golem.entityAIGuardian);
		                    golem.targetTasks.removeTask(golem.entityAISwimming);
		                    golem.targetTasks.removeTask(golem.entityAIMoveTowardsRestriction);
		                    golem.targetTasks.removeTask(golem.entityAIWander);
		                    golem.targetTasks.removeTask(golem.entityAIWatchClosest);
		                    golem.targetTasks.removeTask(golem.entityAILookIdle);
		                    golem.targetTasks.removeTask(golem.entityAIAttackPlayer);
		                    golem.targetTasks.removeTask(golem.entityAIAttackWither);
		                    golem.targetTasks.removeTask(golem.entityAIAttackGolem);
		                    golem.isFarmer = true;
		                }
		                golem.addUpgrade(upgradeId);
		                GuardianGolem.network.sendTo(new SCGolemUpdate(golem.getEntityId(), golem.isFarmer, golem.getOwner(), golem.getXp(), golem.getLevel(), golem.getSubLevel(), golem.getCapacityPoints(), golem.getHealth(), golem.getEntityAttribute(SharedMonsterAttributes.maxHealth).getBaseValue(), golem.getEntityAttribute(SharedMonsterAttributes.attackDamage).getBaseValue(), golem.hasCustomName() ? golem.getName() : "Guardian Golem", golem.getUpgrades(), golem.getLastKnownOwnerName()), player);
		            }
		        } else if (message.instruction == 100) {
		            int golemId;
		            if (message.params.length < 1) {
		                return null;
		            }
		            try {
		                golemId = Integer.parseInt(message.params[0]);
		            }
		            catch (NumberFormatException e) {
		                return null;
		            }
		            Entity golemEntity = player.worldObj.getEntityByID(golemId);
		            if (golemEntity != null && golemEntity instanceof EntityGolem && ((EntityGolem)golemEntity).getOwner().equals(ctx.getServerHandler().playerEntity.getGameProfile().getId().toString())) {
		                EntityGolem golem = (EntityGolem)golemEntity;
		                int point = golem.getUpgradeLevel(28);
		                if (point != 0) {
		                    golem.addCapacityPoints(point);
		                    golem.getUpgrades().remove(28);
		                    golem.tasks.addTask(0, golem.entityAIGuardian);
		                    golem.tasks.addTask(1, golem.entityAISwimming);
		                    golem.tasks.addTask(5, golem.entityAIMoveTowardsRestriction);
		                    golem.tasks.addTask(7, golem.entityAIWander);
		                    golem.tasks.addTask(8, golem.entityAIWatchClosest);
		                    golem.tasks.addTask(8, golem.entityAILookIdle);
		                    golem.targetTasks.addTask(1, golem.entityAIAttackPlayer);
		                    golem.targetTasks.addTask(2, golem.entityAIAttackWither);
		                    golem.targetTasks.addTask(3, golem.entityAIAttackGolem);
		                    golem.isFarmer = false;
		                }
		                GuardianGolem.network.sendTo(new SCGolemUpdate(golem.getEntityId(), golem.isFarmer, golem.getOwner(), golem.getXp(), golem.getLevel(), golem.getSubLevel(), golem.getCapacityPoints(), golem.getHealth(), golem.getEntityAttribute(SharedMonsterAttributes.maxHealth).getBaseValue(), golem.getEntityAttribute(SharedMonsterAttributes.attackDamage).getBaseValue(), golem.hasCustomName() ? golem.getName() : "Guardian Golem", golem.getUpgrades(), golem.getLastKnownOwnerName()), player);
		            }
		        }
		        return null;
		    }
  }
}
