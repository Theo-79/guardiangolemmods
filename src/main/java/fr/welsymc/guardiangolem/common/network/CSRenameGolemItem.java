package fr.welsymc.guardiangolem.common.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.util.ChatComponentText;

public class CSRenameGolemItem implements IMessage
{
  public String name;
  
  public CSRenameGolemItem() {}
  
  public CSRenameGolemItem(String name)
  {
    this.name = name;
  }
  
  public void fromBytes(ByteBuf buf)
  {
    this.name = ByteBufUtils.readUTF8String(buf);
  }
  

  public void toBytes(ByteBuf buf)
  {
    ByteBufUtils.writeUTF8String(buf, this.name);
  }
  
  public static class Handler implements cpw.mods.fml.common.network.simpleimpl.IMessageHandler<CSRenameGolemItem, IMessage>
  {
    public Handler() {}
    
    public IMessage onMessage(CSRenameGolemItem message, MessageContext ctx)
    {
      if (ctx.getServerHandler().playerEntity != null)
      {
        EntityPlayer player = ctx.getServerHandler().playerEntity;
        
        if ((player.inventory.getCurrentItem() != null) && ((player.inventory.getCurrentItem().getItem() instanceof fr.welsymc.guardiangolem.common.items.ItemGuardianRenamer)))
        {
          NBTTagCompound tag = new NBTTagCompound();
          if (message.name.length() > 48)
          {
            player.addChatMessage(new ChatComponentText("Erreur, le texte doit faire moins de 48 lettres"));
            return null;
          }
          
          tag.setString("golemName", message.name);
          player.inventory.getCurrentItem().setTagCompound(tag);
        }
      }
      
      return null;
    }
  }
}
