package fr.welsymc.guardiangolem.common.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.world.World;

public class CSOpenTree implements IMessage
{
  public int playerId;
  public int guiId;
  public int entityId;
  
  public CSOpenTree() {}
  
  public CSOpenTree(int guiId, int entityId)
  {
    this.guiId = guiId;
    this.entityId = entityId;
  }
  
  public void fromBytes(ByteBuf buf)
  {
    this.guiId = buf.readInt();
    this.entityId = buf.readInt();
  }
  
  public void toBytes(ByteBuf buf)
  {
    buf.writeInt(this.guiId);
    buf.writeInt(this.entityId);
  }
  
  public static class Handler implements IMessageHandler<CSOpenTree, IMessage> {
    public Handler() {}
    
    public IMessage onMessage(CSOpenTree message, MessageContext ctx) {
      EntityPlayer p = ctx.getServerHandler().playerEntity;
      if ((p.worldObj.getEntityByID(message.entityId) != null) && ((p.worldObj.getEntityByID(message.entityId) instanceof EntityGolem))) {
        EntityGolem entityGolem = (EntityGolem)p.worldObj.getEntityByID(message.entityId);
        p.openGui(GuardianGolem.instance, message.guiId, p.worldObj, entityGolem.getEntityId(), 0, 0);
      }
      return null;
    }
  }
}
