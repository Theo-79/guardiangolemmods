package fr.welsymc.guardiangolem.common.events;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import fr.welsymc.guardiangolem.client.gui.components.GuiBarUnified;
import fr.welsymc.guardiangolem.client.renderer.BossStatusGuardian;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.utils.LibRessources;
import fr.welsymc.guardiangolem.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

public class EventHandler
{
  public EventHandler() {}
  
  @SubscribeEvent
  public void renderBosBar(RenderGameOverlayEvent.Pre e)
  {
    if ((e.type == RenderGameOverlayEvent.ElementType.HOTBAR) && 
      (BossStatusGuardian.bossName != null) && (BossStatusGuardian.statusBarTime > 0) && 
      (!BossStatusGuardian.guardian.hasUpgrade(26))) {
      Minecraft mc = Minecraft.getMinecraft();
      
      BossStatusGuardian.statusBarTime -= 1;
      FontRenderer fontrenderer = mc.fontRenderer;
      ScaledResolution scaledresolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
      int i = scaledresolution.getScaledWidth();
      short short1 = 182;
      int j = i / 2 - short1 / 2;
      int k = (int)(BossStatusGuardian.healthScale * (short1 + 1));
      byte b0 = 12;
      
      GuiBarUnified bar = new GuiBarUnified((scaledresolution.getScaledWidth() - 200) / 2, 20, 200, LibRessources.UNIFIED_BAR_RED, 1.0D);
      
      bar.setScaledBar((int)BossStatusGuardian.guardian.getHealth(), (int)BossStatusGuardian.guardian.getMaxHealth());
      bar.drawBar(false);
      
      if (ClientProxy.hasWither) {
        return;
      }
      
      String s = BossStatusGuardian.bossName;
      fontrenderer.drawStringWithShadow(s, i / 2 - fontrenderer.getStringWidth(s) / 2, b0 - 10, 16777215);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    }
  }
  




  @SubscribeEvent
  public void onEntityAttacked(LivingAttackEvent e)
  {
    if ((e.entity instanceof EntityGolem)) {
      EntityGolem golem = (EntityGolem)e.entity;
      int level = golem.getUpgradeLevel(6);
      if (level == 0)
        return;
      if ((e.source.getEntity() != null) && 
        ((e.source.getEntity() instanceof EntityPlayer))) {
        if (((EntityPlayer)e.source.getEntity()).getGameProfile().getName().toLowerCase().equals(golem.getOwner().toLowerCase())) {
          return;
        }
        e.source.getEntity().attackEntityFrom(DamageSource.magic, level * 2.0F);
      }
    }
  }
}
