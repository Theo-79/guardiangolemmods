package fr.welsymc.guardiangolem.common.blocks;

import com.mojang.authlib.GameProfile;
import fr.paladium.palamod.api.BlocksRegister;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.init.ItemInit;
import fr.welsymc.guardiangolem.common.items.ItemGuardianStone;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BlockGuardian extends Block
{
  public BlockGuardian()
  {
    super(net.minecraft.block.material.Material.rock);
    setBlockName("guardian_block");
    setBlockTextureName("guardiangolem:guardian_block");
    setCreativeTab(GuardianGolem.TAB);
    setHardness(3.5F);
  }
  


  public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer p, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_)
  {
    if ((p.inventory.getCurrentItem() != null) && (p.inventory.getCurrentItem().isItemEqual(new ItemStack(ItemInit.GUARDIAN_STONE))))
    {
      if (w.isRemote) { return true;
      }
      
      boolean hasReplaced = false;
      
      Block xplus = w.getBlock(x + 1, y, z);
      Block xmoins = w.getBlock(x - 1, y, z);
      if ((xplus == BlocksRegister.BLOCK_PALADIUM) && (xmoins == BlocksRegister.BLOCK_PALADIUM) && (w.getBlock(x, y - 1, z) == BlocksRegister.BLOCK_PALADIUM))
      {
        hasReplaced = true;
        w.setBlockToAir(x + 1, y, z);
        w.setBlockToAir(x - 1, y, z);
        w.setBlockToAir(x, y - 1, z);
        w.setBlockToAir(x, y, z);
        
        spawnGolem(w, x, y, z, p);
      }
      
      if (!hasReplaced)
      {
        Block zplus = w.getBlock(x, y, z + 1);
        Block zmoins = w.getBlock(x, y, z - 1);
        if ((zplus == BlocksRegister.BLOCK_PALADIUM) && (zmoins == BlocksRegister.BLOCK_PALADIUM) && (w.getBlock(x, y - 1, z) == BlocksRegister.BLOCK_PALADIUM))
        {
          w.setBlockToAir(x, y, z + 1);
          w.setBlockToAir(x, y, z - 1);
          w.setBlockToAir(x, y - 1, z);
          w.setBlockToAir(x, y, z);
          spawnGolem(w, x, y, z, p);
          hasReplaced = true;
        }
      }
    }
    
    return false;
  }
  
  private void spawnGolem(World w, int x, int y, int z, EntityPlayer p) {
    EntityGolem golly = new EntityGolem(w);
    ItemGuardianStone.getGolemFromStone(p.inventory.getCurrentItem(), golly);
    golly.setPosition(x, y, z);
    golly.setOwner(p.getGameProfile().getId().toString());
    golly.setHealth(golly.getMaxHealth());
    w.spawnEntityInWorld(golly);
    p.inventory.getCurrentItem().stackSize -= 1;
  }
}
