package fr.welsymc.guardiangolem.common.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.welsymc.guardiangolem.GuardianGolem;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;



public class BlockGuardianKeeper
  extends Block
{
  @SideOnly(Side.CLIENT)
  protected IIcon haut;
  @SideOnly(Side.CLIENT)
  protected IIcon coter;
  
  public BlockGuardianKeeper()
  {
    super(Material.rock);
    setBlockName("guardian_keeper");
    setBlockTextureName("guardiangolem:guardian_keeper");
    setCreativeTab(GuardianGolem.TAB);
    setHardness(3.5F);
  }
  
  @SideOnly(Side.CLIENT)
  public void registerBlockIcons(IIconRegister r)
  {
    this.haut = r.registerIcon("guardiangolem:guardian_keeper_top");
    this.coter = r.registerIcon("guardiangolem:guardian_keeper");
  }
  



  public boolean onBlockActivated(World p_149727_1_, int p_149727_2_, int p_149727_3_, int p_149727_4_, EntityPlayer p_149727_5_, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_)
  {
    if (!p_149727_1_.isRemote) {
      p_149727_5_.openGui(GuardianGolem.instance, 2, p_149727_1_, p_149727_2_, p_149727_3_, p_149727_4_);
    }
    
    return true;
  }
  

  @SideOnly(Side.CLIENT)
  public IIcon getIcon(int side, int meta)
  {
    if ((side == 0) || (side == 1)) return this.haut;
    return this.coter;
  }
}
