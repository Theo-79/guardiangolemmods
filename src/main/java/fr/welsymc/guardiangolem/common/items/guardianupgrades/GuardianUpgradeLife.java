package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeLife extends GuardianUpgradeItem
{
  public GuardianUpgradeLife()
  {
    super(0, new ItemStack(Items.golden_apple), "guardian.upgrade.name.tank", "guardian.upgrade.desc.tank", 3, 1, 350, 300);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    int base = 100 + 100 * golem.getLevel();
    int percentage = (base / 3 + base) * level;
    golem.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(percentage);
  }
}
