package fr.welsymc.guardiangolem.common.items.cosmetics.hats;

import fr.welsymc.guardiangolem.client.renderer.cosmetics.ICosmeticHatModel;
import fr.welsymc.guardiangolem.client.renderer.cosmetics.ModelFarmerCosmetic;
import fr.welsymc.guardiangolem.common.items.ItemCosmetic;
import net.minecraft.client.model.ModelBase;
import net.minecraft.util.ResourceLocation;

public class ItemFarmerCosmetic
  extends ItemCosmetic implements ICosmeticHat
{
  private ResourceLocation texture = new ResourceLocation("guardiangolem", "textures/cosmetics/hats/farmer.png");
  private ModelBase model = new ModelFarmerCosmetic();
  
  public ItemFarmerCosmetic(String name) {
    super(name);
  }
  
  public ICosmeticHatModel model()
  {
    return (ICosmeticHatModel)this.model;
  }
  
  public ResourceLocation texture()
  {
    return this.texture;
  }
}
