package fr.welsymc.guardiangolem.common.items.cosmetics.hats;

import fr.welsymc.guardiangolem.client.renderer.cosmetics.ICosmeticHatModel;
import net.minecraft.util.ResourceLocation;

public abstract interface ICosmeticHat
{
  public abstract ResourceLocation texture();
  
  public abstract ICosmeticHatModel model();
}
