package fr.welsymc.guardiangolem.common.items;

import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.common.utils.CosmeticType;
import net.minecraft.item.Item;

public class ItemCosmetic extends Item
{
  private String name;
  private CosmeticType type;
  
  public ItemCosmetic(String name)
  {
    setUnlocalizedName(name);
    setTextureName("guardiangolem:" + name);
    setMaxStackSize(1);
    setCreativeTab(GuardianGolem.TAB);
  }
  
  public ItemCosmetic setName(String name) {
    this.name = name;
    return this;
  }
  
  public ItemCosmetic setType(CosmeticType type) {
    this.type = type;
    return this;
  }
  
  public String getName() {
    return this.name;
  }
  
  public CosmeticType getType() {
    return this.type;
  }
}
