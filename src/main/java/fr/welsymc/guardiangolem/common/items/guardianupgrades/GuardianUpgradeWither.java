package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeWither extends GuardianUpgradeItem
{
  public GuardianUpgradeWither()
  {
    super(16, new ItemStack(Items.skull, 1, 1), "guardian.upgrade.name.wither", "guardian.upgrade.desc.wither", 1, 1, 300, 200);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
