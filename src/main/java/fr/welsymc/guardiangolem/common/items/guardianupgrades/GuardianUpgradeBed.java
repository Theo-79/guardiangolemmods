package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeBed
  extends GuardianUpgradeItem
{
  public GuardianUpgradeBed()
  {
    super(18, new ItemStack(Items.paper), "guardian.upgrade.name.bed", "guardian.upgrade.desc.bed", 3, 1, 450, 150);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
