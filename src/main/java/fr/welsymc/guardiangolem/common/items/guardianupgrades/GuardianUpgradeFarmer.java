package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.paladium.palamod.api.ItemsRegister;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import java.util.ArrayList;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class GuardianUpgradeFarmer extends GuardianUpgradeItem
{
  public GuardianUpgradeFarmer()
  {
    super(28, new ItemStack(Items.diamond_hoe), "guardian.upgrade.name.farmer", "guardian.upgrade.desc.farmer", 3, 1, 250, 400);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    if ((!golem.worldObj.isRemote) && 
      (golem.worldObj.getTotalWorldTime() % 36000L == 0L)) {
      ArrayList<ItemStack> stacks = new ArrayList();
      if (level >= 1) {
        stacks.add(new ItemStack(Items.diamond, 1));
        stacks.add(new ItemStack(Items.iron_ingot, 1));
        stacks.add(new ItemStack(Items.gold_ingot, 1));
      }
      if (level >= 2) {
        stacks.add(new ItemStack(ItemsRegister.TITANE_INGOT, 1));
        stacks.add(new ItemStack(ItemsRegister.AMETHYST_INGOT, 1));
      }
      if (level >= 3) {
        stacks.add(new ItemStack(ItemsRegister.PALADIUM_INGOT, 1));
      }
      
      int index = golem.worldObj.rand.nextInt(stacks.size());
      
      EntityItem entityItem = new EntityItem(golem.worldObj, golem.posX, golem.posY, golem.posZ, (ItemStack)stacks.get(index));
      golem.worldObj.spawnEntityInWorld(entityItem);
    }
  }
}
