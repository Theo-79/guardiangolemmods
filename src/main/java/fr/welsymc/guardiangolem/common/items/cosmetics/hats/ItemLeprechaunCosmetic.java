package fr.welsymc.guardiangolem.common.items.cosmetics.hats;

import fr.welsymc.guardiangolem.client.renderer.cosmetics.ICosmeticHatModel;
import fr.welsymc.guardiangolem.client.renderer.cosmetics.ModelLeprechaunCosmetic;
import fr.welsymc.guardiangolem.common.items.ItemCosmetic;
import net.minecraft.client.model.ModelBase;
import net.minecraft.util.ResourceLocation;

public class ItemLeprechaunCosmetic
  extends ItemCosmetic implements ICosmeticHat
{
  private ResourceLocation texture = new ResourceLocation("guardiangolem", "textures/cosmetics/hats/leprechaun.png");
  private ModelBase model = new ModelLeprechaunCosmetic();
  
  public ItemLeprechaunCosmetic(String name) {
    super(name);
  }
  
  public ICosmeticHatModel model()
  {
    return (ICosmeticHatModel)this.model;
  }
  
  public ResourceLocation texture()
  {
    return this.texture;
  }
}
