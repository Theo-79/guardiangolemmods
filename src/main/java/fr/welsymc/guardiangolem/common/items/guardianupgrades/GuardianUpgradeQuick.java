package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.utils.GuardianUtils;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;

public class GuardianUpgradeQuick extends GuardianUpgradeItem
{
  public GuardianUpgradeQuick()
  {
    super(14, new ItemStack(Items.sugar), "guardian.upgrade.name.quick", "guardian.upgrade.desc.quick", 1, 1, 450, 350);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    if (golem.worldObj.getWorldTime() % 80L == 0L) {
      GuardianUtils.setEffectToGolem(golem, 1, Potion.moveSpeed, true);
    }
  }
}
