package fr.welsymc.guardiangolem.common.items;

import fr.welsymc.guardiangolem.GuardianGolem;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemGuardianHammer
  extends Item
{
  public ItemGuardianHammer()
  {
    setUnlocalizedName("guardianhammer");
    setTextureName("guardiangolem:guardianhammer");
    setCreativeTab(GuardianGolem.TAB);
    setMaxStackSize(1);
    setMaxDamage(2);
  }
  
  public void addInformation(ItemStack p_77624_1_, EntityPlayer p, List l, boolean p_77624_4_)
  {
    if (l != null) {
      l.add("Permet de tuer instantan�ment");
      l.add("Le golem dont vous �tes propri�taire");
      l.add("�cNe marche que 3 fois !");
    }
  }
}
