package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeLifesteal extends GuardianUpgradeItem
{
  public GuardianUpgradeLifesteal()
  {
    super(9, new ItemStack(Items.diamond_sword), "guardian.upgrade.name.lifesteal", "guardian.upgrade.desc.lifesteal", 3, 1, 200, 450);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
