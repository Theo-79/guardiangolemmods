package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeSecret extends GuardianUpgradeItem
{
  public GuardianUpgradeSecret()
  {
    super(26, new ItemStack(Items.apple), "guardian.upgrade.name.secret", "guardian.upgrade.desc.secret", 1, 1, 200, 350);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
