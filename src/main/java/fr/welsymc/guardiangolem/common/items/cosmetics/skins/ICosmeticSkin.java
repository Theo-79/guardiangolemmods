package fr.welsymc.guardiangolem.common.items.cosmetics.skins;

import net.minecraft.util.ResourceLocation;

public abstract interface ICosmeticSkin
{
  public abstract ResourceLocation texture();
}
