package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeBKiller extends GuardianUpgradeItem
{
  public GuardianUpgradeBKiller()
  {
    super(23, new ItemStack(Items.apple), "guardian.upgrade.name.bkiller", "guardian.upgrade.desc.bkiller", 3, 1, 300, 500);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
