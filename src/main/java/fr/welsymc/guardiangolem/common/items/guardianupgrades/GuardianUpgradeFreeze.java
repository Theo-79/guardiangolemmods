package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeFreeze extends GuardianUpgradeItem
{
  public GuardianUpgradeFreeze()
  {
    super(10, new ItemStack(Blocks.ice), "guardian.upgrade.name.freeze", "guardian.upgrade.desc.freeze", 3, 1, 250, 500);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
