package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeWhistle extends GuardianUpgradeItem
{
  public GuardianUpgradeWhistle()
  {
    super(19, new ItemStack(Blocks.bedrock), "guardian.upgrade.name.whistle", "guardian.upgrade.desc.whistle", 3, 1, 450, 250);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
