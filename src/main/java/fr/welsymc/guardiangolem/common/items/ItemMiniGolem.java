package fr.welsymc.guardiangolem.common.items;

import com.mojang.authlib.GameProfile;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import java.util.List;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;

public class ItemMiniGolem extends Item
{
  public ItemMiniGolem()
  {
    setCreativeTab(GuardianGolem.TAB);
    setUnlocalizedName("mini_golem");
    setTextureName("guardiangolem:mini_golem");
    setMaxStackSize(1);
  }
  
  public static void getGolemFromMiniGolem(ItemStack item, EntityGolem entity) {
    if (entity == null)
      return;
    if ((item == null) || (!(item.getItem() instanceof ItemMiniGolem)))
      return;
    if (!item.hasTagCompound())
      return;
    if (!item.getTagCompound().hasKey("owner")) {
      return;
    }
    
    NBTTagCompound comp = item.getTagCompound();
    entity.readEntityFromNBT(comp);
  }
  

  public boolean onItemUse(ItemStack item, EntityPlayer player, World world, int x, int y, int z, int hitX, float hitY, float hitZ, float p_77648_10_)
  {
    if ((item.hasTagCompound()) && (!world.isRemote) && 
      (item.getTagCompound().hasKey("owner")) && (canPlace(player, x, y, z))) {
      EntityGolem golly = new EntityGolem(world);
      getGolemFromMiniGolem(item, golly);
      golly.setPosition(x, y + 1, z);
      golly.setHealth(golly.getMaxHealth());
      golly.setOwner(player.getGameProfile().getId().toString());
      world.spawnEntityInWorld(golly);
      item.stackSize -= 1;
      return true;
    }
    
    return false;
  }
  

  public static boolean canPlace(EntityPlayer player, int x, int y, int z)
  {
    BlockSnapshot snapshot = new BlockSnapshot(player.worldObj, x, y, z, Blocks.cobblestone, 0);
    if (player.worldObj.getBlock(x, y, z) == Blocks.bedrock) {
      return false;
    }
    return 
      !ForgeEventFactory.onPlayerBlockPlace(player, snapshot, ForgeDirection.getOrientation(0)).isCanceled();
  }
  
  public void addInformation(ItemStack item, EntityPlayer player, List list, boolean flag)
  {
    if (item.hasTagCompound()) {
      if (item.getTagCompound().hasKey("owner")) {
        list.add("�aContient un Guardian Golem de niveau �e" + item.getTagCompound().getInteger("level"));
      } else {
        list.add("�cCe Mini Golem ne contient aucune �eGuardian Stone");
      }
    } else {
      list.add("�cCe Mini Golem ne contient aucune �eGuardian Stone");
    }
    super.addInformation(item, player, list, flag);
  }
}
