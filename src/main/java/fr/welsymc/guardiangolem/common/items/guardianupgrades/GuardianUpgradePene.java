package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradePene extends GuardianUpgradeItem
{
  public GuardianUpgradePene()
  {
    super(5, new ItemStack(Items.diamond_axe), "guardian.upgrade.name.pene", "guardian.upgrade.desc.pene", 3, 1, 250, 450);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
