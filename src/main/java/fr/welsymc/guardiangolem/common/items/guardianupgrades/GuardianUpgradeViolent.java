package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeViolent extends GuardianUpgradeItem
{
  public GuardianUpgradeViolent()
  {
    super(3, new ItemStack(Items.diamond_sword), "guardian.upgrade.name.violent", "guardian.upgrade.desc.violent", 3, 1, 300, 400);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    double base = 6.0D + golem.getLevel();
    double percentage = base / 30.0D + base;
    golem.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(percentage * level * 2.0D);
  }
}
