package fr.welsymc.guardiangolem.common.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.client.gui.GuiRenameGolem;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemGuardianRenamer extends Item
{
  public ItemGuardianRenamer()
  {
    setUnlocalizedName("guardian_renamer");
    setTextureName("guardiangolem:guardian_renamer");
    setMaxStackSize(1);
    setCreativeTab(GuardianGolem.TAB);
  }
  
  @SideOnly(Side.CLIENT)
  public ItemStack onItemRightClick(ItemStack stack, World w, EntityPlayer p)
  {
    if (w.isRemote)
    {
      if (stack.hasTagCompound())
      {
        Minecraft.getMinecraft().displayGuiScreen(new GuiRenameGolem(stack.getTagCompound().getString("golemName")));
      } else {
        Minecraft.getMinecraft().displayGuiScreen(new GuiRenameGolem(""));
      }
    }
    
    return stack;
  }
  
  public void addInformation(ItemStack s, EntityPlayer p_77624_2_, List l, boolean p_77624_4_)
  {
    l.add("Nom d�fini actuellement:");
    l.add((s.hasTagCompound()) && (s.getTagCompound().getString("golemName").trim().length() > 0) ? s.getTagCompound().getString("golemName") : "Aucun");
    l.add(" ");
    l.add("Clic droit pour ouvrir l'interface");
  }
}
