package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeClean extends GuardianUpgradeItem
{
  public GuardianUpgradeClean()
  {
    super(22, new ItemStack(Blocks.dirt), "guardian.upgrade.name.clean", "guardian.upgrade.desc.clean", 1, 1, 550, 250);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
