package fr.welsymc.guardiangolem.common.items;

import fr.welsymc.guardiangolem.GuardianGolem;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumChatFormatting;


public class ItemWhitelist
  extends Item
{
  public ItemWhitelist()
  {
    setUnlocalizedName("whitelist");
    setTextureName("guardiangolem:whitelist");
    setCreativeTab(GuardianGolem.TAB);
    setMaxStackSize(1);
  }
  
  public void addInformation(ItemStack stack, EntityPlayer p_77624_2_, List list, boolean p_77624_4_)
  {
    super.addInformation(stack, p_77624_2_, list, p_77624_4_);
    list.add("Joueurs autorisÚs:");
    
    if ((stack.hasTagCompound()) && (stack.getTagCompound().hasKey("players")))
    {
      NBTTagList players = stack.getTagCompound().getTagList("players", 8);
      for (int i = 0; i < players.tagCount(); i++)
      {
        list.add(EnumChatFormatting.GOLD + players.getStringTagAt(i));
      }
    }
    else {
      list.add("Aucun");
    }
    
    list.add("");
    list.add(EnumChatFormatting.GRAY + "(/whitelistgolem avec l'item en main)");
  }
}
