package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import java.util.Random;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeAutoXp extends GuardianUpgradeItem
{
  public GuardianUpgradeAutoXp()
  {
    super(27, new ItemStack(net.minecraft.init.Items.experience_bottle), "guardian.upgrade.name.autoxp", "guardian.upgrade.desc.autoxp", 1, 1, 250, 350);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    int r = golem.worldObj.rand.nextInt(176);
    if (r == 35)
    {
      golem.addXP(1 + (int)golem.worldObj.rand.nextFloat());
    }
  }
}
