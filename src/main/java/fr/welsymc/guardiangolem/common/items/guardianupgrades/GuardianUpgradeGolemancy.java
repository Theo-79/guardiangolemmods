package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeGolemancy extends GuardianUpgradeItem
{
  public GuardianUpgradeGolemancy()
  {
    super(2, new ItemStack(Items.apple), "guardian.upgrade.name.golemancy", "guardian.upgrade.desc.golemancy", 1, 1, 300, 300);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
