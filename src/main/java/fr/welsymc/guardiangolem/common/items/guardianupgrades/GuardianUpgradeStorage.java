package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeStorage extends GuardianUpgradeItem
{
  public GuardianUpgradeStorage()
  {
    super(21, new ItemStack(Blocks.chest), "guardian.upgrade.name.storage", "guardian.upgrade.desc.storage", 3, 1, 500, 250);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
