package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeExplode extends GuardianUpgradeItem
{
  public GuardianUpgradeExplode()
  {
    super(20, new ItemStack(Blocks.tnt), "guardian.upgrade.name.explode", "guardian.upgrade.desc.explode", 3, 1, 500, 200);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
