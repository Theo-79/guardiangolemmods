package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeThorns extends GuardianUpgradeItem
{
  public GuardianUpgradeThorns()
  {
    super(6, new ItemStack(Blocks.cactus), "guardian.upgrade.name.thorns", "guardian.upgrade.desc.thorns", 3, 1, 350, 350);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
