package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeAnchor extends GuardianUpgradeItem
{
  public GuardianUpgradeAnchor()
  {
    super(17, new ItemStack(Items.apple), "guardian.upgrade.name.anchor", "guardian.upgrade.desc.anchor", 3, 1, 450, 200);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
