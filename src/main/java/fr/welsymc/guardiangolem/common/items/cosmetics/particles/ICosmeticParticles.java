package fr.welsymc.guardiangolem.common.items.cosmetics.particles;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;

public abstract interface ICosmeticParticles
{
  public abstract void update(EntityGolem paramEntityGolem);
}
