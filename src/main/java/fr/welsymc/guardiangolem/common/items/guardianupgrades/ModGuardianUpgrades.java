package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import java.util.HashMap;

public class ModGuardianUpgrades
{
  public static HashMap<Integer, GuardianUpgradeItem> list;
  
  public ModGuardianUpgrades() {}
  
  public static void init() {
    list = new HashMap();
    
    GuardianUpgradeItem life = new GuardianUpgradeLife();
    GuardianUpgradeItem beacon = new GuardianUpgradeBeacon();
    GuardianUpgradeItem golemancy = new GuardianUpgradeGolemancy();
    GuardianUpgradeItem violent = new GuardianUpgradeViolent();
    GuardianUpgradeItem gluton = new GuardianUpgradeGluton();
    GuardianUpgradeItem pene = new GuardianUpgradePene();
    GuardianUpgradeItem thorns = new GuardianUpgradeThorns();
    GuardianUpgradeItem range = new GuardianUpgradeRange();
    GuardianUpgradeItem weapon = new GuardianUpgradeWeapon();
    GuardianUpgradeItem lifesteal = new GuardianUpgradeLifesteal();
    GuardianUpgradeItem freeze = new GuardianUpgradeFreeze();
    GuardianUpgradeItem berzerk = new GuardianUpgradeBerzerk();
    GuardianUpgradeItem resistance = new GuardianUpgradeResistance();
    GuardianUpgradeItem quick = new GuardianUpgradeQuick();
    GuardianUpgradeItem bunny = new GuardianUpgradeBunny();
    GuardianUpgradeItem wither = new GuardianUpgradeWither();
    GuardianUpgradeItem anchor = new GuardianUpgradeAnchor();
    GuardianUpgradeItem bed = new GuardianUpgradeBed();
    GuardianUpgradeItem wblower = new GuardianUpgradeWhistle();
    GuardianUpgradeItem explode = new GuardianUpgradeExplode();
    GuardianUpgradeItem storage = new GuardianUpgradeStorage();
    GuardianUpgradeItem clean = new GuardianUpgradeClean();
    GuardianUpgradeItem bkiller = new GuardianUpgradeBKiller();
    GuardianUpgradeItem canibal = new GuardianUpgradeCanibal();
    GuardianUpgradeItem hiden = new GuardianUpgradeHidden();
    GuardianUpgradeItem secret = new GuardianUpgradeSecret();
    GuardianUpgradeItem autoXP = new GuardianUpgradeAutoXp();
    GuardianUpgradeItem farmer = new GuardianUpgradeFarmer();
    
    life.addParent(golemancy);
    violent.addParent(golemancy);
    wither.addParent(golemancy);
    gluton.addParent(golemancy);
    anchor.addParent(wither);
    bed.addParent(anchor);
    wblower.addParent(anchor);
    explode.addParent(anchor);
    storage.addParent(explode);
    clean.addParent(storage);
    beacon.addParent(life);
    beacon.addParent(anchor);
    berzerk.addParent(beacon);
    berzerk.addParent(violent);
    resistance.addParent(beacon);
    quick.addParent(beacon);
    bunny.addParent(beacon);
    autoXP.addParent(gluton);
    farmer.addParent(autoXP);
    hiden.addParent(gluton);
    secret.addParent(hiden);
    thorns.addParent(life);
    thorns.addParent(violent);
    range.addParent(violent);
    weapon.addParent(pene);
    lifesteal.addParent(pene);
    freeze.addParent(pene);
    pene.addParent(violent);
    bkiller.addParent(violent);
    canibal.addParent(bkiller);
    
    addItem(0, life);
    addItem(1, beacon);
    addItem(2, golemancy);
    addItem(3, violent);
    addItem(4, gluton);
    addItem(5, pene);
    addItem(6, thorns);
    addItem(7, range);
    addItem(8, weapon);
    addItem(9, lifesteal);
    addItem(10, freeze);
    addItem(11, berzerk);
    addItem(12, resistance);
    addItem(14, quick);
    addItem(15, bunny);
    addItem(16, wither);
    addItem(17, anchor);
    addItem(18, bed);
    addItem(19, wblower);
    addItem(20, explode);
    addItem(21, storage);
    addItem(22, clean);
    addItem(23, bkiller);
    addItem(24, canibal);
    addItem(25, hiden);
    addItem(26, secret);
    addItem(27, autoXP);
    addItem(28, farmer);
    System.out.println(list.size());
  }
  
  public static void addItem(int id, GuardianUpgradeItem item) {
    list.put(Integer.valueOf(id), item);
  }
  
  public static GuardianUpgradeItem get(int id) {
    return (GuardianUpgradeItem)list.getOrDefault(Integer.valueOf(id), null);
  }
}
