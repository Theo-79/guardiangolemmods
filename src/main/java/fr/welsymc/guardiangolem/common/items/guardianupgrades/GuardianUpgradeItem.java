package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import java.util.ArrayList;
import net.minecraft.item.ItemStack;



public abstract class GuardianUpgradeItem
{
  String name;
  String desc;
  ItemStack stack;
  int maxLevel;
  int pointPerLevel;
  int id;
  int posX;
  int posY;
  ArrayList<GuardianUpgradeItem> parents;
  
  public GuardianUpgradeItem(int id, ItemStack stack, String name, String desc, int maxLevel, int pointPerLevel, int posX, int posY)
  {
    this.id = id;
    this.name = name;
    this.stack = stack;
    this.desc = desc;
    this.maxLevel = maxLevel;
    this.pointPerLevel = pointPerLevel;
    this.posX = posX;
    this.posY = posY;
    this.parents = new ArrayList();
  }
  
  public GuardianUpgradeItem(int id, ItemStack stack, String name, String desc, int maxLevel, int pointPerLevel, int posX, int posY, ArrayList<GuardianUpgradeItem> parents) {
    this(id, stack, name, desc, maxLevel, pointPerLevel, posX, posY);
    this.parents = parents;
  }
  
  public void addParent(GuardianUpgradeItem item) {
    this.parents.add(item);
  }
  
  public ItemStack getItemStack(int level) {
    return this.stack;
  }
  
  public abstract void updateGolem(int paramInt, EntityGolem paramEntityGolem);
  
  public int getId() {
    return this.id;
  }
  
  public String getName() {
    return this.name;
  }
  
  public String getDesc() {
    return this.desc;
  }
  
  public ArrayList<GuardianUpgradeItem> getParents() {
    return this.parents;
  }
  
  public int getX() {
    return this.posX;
  }
  
  public int getY() {
    return this.posY;
  }
  
  public int getMaxLevel() {
    return this.maxLevel;
  }
  
  public int getCapacityPoints() {
    return this.pointPerLevel;
  }
}
