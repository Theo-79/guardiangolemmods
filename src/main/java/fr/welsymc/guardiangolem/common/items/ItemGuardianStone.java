package fr.welsymc.guardiangolem.common.items;

import fr.welsymc.guardiangolem.GuardianGolem;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.init.ItemInit;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ItemGuardianStone
  extends Item
{
  public ItemGuardianStone()
  {
    setUnlocalizedName("guardian_stone");
    setTextureName("guardiangolem:guardian_stone");
    setMaxStackSize(1);
    setCreativeTab(GuardianGolem.TAB);
  }
  
  public static ItemStack getGuardianStone(EntityGolem golem) {
    ItemStack stack = new ItemStack(ItemInit.GUARDIAN_STONE, 1);
    if (golem != null) {
      NBTTagCompound comp = new NBTTagCompound();
      comp = golem.getCompound(comp);
      stack.setTagCompound(comp);
    }
    return stack;
  }
  
  public static void getGolemFromStone(ItemStack stone, EntityGolem golem) {
    if (golem == null)
      return;
    if ((stone == null) || (!(stone.getItem() instanceof ItemGuardianStone)))
      return;
    if (!stone.hasTagCompound()) {
      return;
    }
    NBTTagCompound comp = stone.getTagCompound();
    golem.readEntityFromNBT(comp);
  }
  
  public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean bool)
  {
    if (stack.hasTagCompound())
    {
      NBTTagCompound comp = stack.getTagCompound();
      list.add("Habit� par l'�me d'un golem");
      list.add("de niveau �c" + comp.getInteger("level"));
    }
    else {
      list.add("Habit� par l'�me d'aucun golem");
    }
  }
}
