package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.utils.GuardianUtils;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;

public class GuardianUpgradeBunny extends GuardianUpgradeItem
{
  public GuardianUpgradeBunny()
  {
    super(15, new ItemStack(Items.feather), "guardian.upgrade.name.bunny", "guardian.upgrade.desc.bunny", 3, 1, 500, 350);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    if (golem.worldObj.getWorldTime() % 80L == 0L) {
      GuardianUtils.setEffectToGolem(golem, 2, Potion.jump, true);
    }
  }
}
