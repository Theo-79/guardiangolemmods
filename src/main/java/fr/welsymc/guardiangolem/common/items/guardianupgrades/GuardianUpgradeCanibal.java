package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeCanibal extends GuardianUpgradeItem
{
  public GuardianUpgradeCanibal()
  {
    super(24, new ItemStack(Items.apple), "guardian.upgrade.name.canibal", "guardian.upgrade.desc.canibal", 3, 1, 350, 500);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
