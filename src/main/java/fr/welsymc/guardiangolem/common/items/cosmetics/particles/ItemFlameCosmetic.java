package fr.welsymc.guardiangolem.common.items.cosmetics.particles;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;

public class ItemFlameCosmetic extends fr.welsymc.guardiangolem.common.items.ItemCosmetic implements ICosmeticParticles
{
  public ItemFlameCosmetic(String name)
  {
    super(name);
  }
  
  public void update(EntityGolem golem)
  {
    float tickAnimated = golem.ticksExisted / 10.0F;
    golem.worldObj.spawnParticle("flame", golem.posX + Math.cos(tickAnimated % 31.41592653589793D), golem.posY + 2.0D, golem.posZ + Math.sin(tickAnimated % 31.41592653589793D), 0.0D, 0.0D, 0.0D);
  }
}
