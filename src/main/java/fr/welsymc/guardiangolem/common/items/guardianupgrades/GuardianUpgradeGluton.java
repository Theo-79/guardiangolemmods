package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeGluton extends GuardianUpgradeItem
{
  public GuardianUpgradeGluton()
  {
    super(4, new ItemStack(Items.cake), "guardian.upgrade.name.gluton", "guardian.upgrade.desc.gluton", 3, 3, 250, 300);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
