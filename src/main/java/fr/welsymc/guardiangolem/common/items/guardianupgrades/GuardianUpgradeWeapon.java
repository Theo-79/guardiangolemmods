package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeWeapon extends GuardianUpgradeItem
{
  public GuardianUpgradeWeapon()
  {
    super(8, new ItemStack(Items.apple), "guardian.upgrade.name.weapon", "guardian.upgrade.desc.weapon", 3, 1, 200, 500);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
