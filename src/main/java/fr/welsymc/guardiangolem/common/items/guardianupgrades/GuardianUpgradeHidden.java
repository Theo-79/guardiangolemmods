package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class GuardianUpgradeHidden extends GuardianUpgradeItem
{
  public GuardianUpgradeHidden()
  {
    super(25, new ItemStack(Items.golden_carrot), "guardian.upgrade.name.hidden", "guardian.upgrade.desc.hidden", 3, 1, 200, 300);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem) {}
}
