package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.utils.GuardianUtils;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.world.World;

public class GuardianUpgradeResistance extends GuardianUpgradeItem
{
  public GuardianUpgradeResistance()
  {
    super(12, new ItemStack(net.minecraft.init.Blocks.chest), "guardian.upgrade.name.resistance", "guardian.upgrade.desc.resistance", 1, 1, 400, 350);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    if (golem.worldObj.getWorldTime() % 80L == 0L) {
      GuardianUtils.setEffectToGolem(golem, 1, Potion.resistance, true);
    }
  }
}
