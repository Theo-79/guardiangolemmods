package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.utils.GuardianUtils;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;

public class GuardianUpgradeBerzerk extends GuardianUpgradeItem
{
  public GuardianUpgradeBerzerk()
  {
    super(11, new ItemStack(Items.apple), "guardian.upgrade.name.berzerk", "guardian.upgrade.desc.berzerk", 1, 1, 350, 400);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    if (golem.worldObj.getWorldTime() % 80L == 0L) {
      GuardianUtils.setEffectToGolem(golem, 1, Potion.damageBoost, true);
    }
  }
}
