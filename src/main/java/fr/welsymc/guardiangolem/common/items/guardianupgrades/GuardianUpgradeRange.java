package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import com.mojang.authlib.GameProfile;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.items.ItemWhitelist;
import java.util.Random;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class GuardianUpgradeRange extends GuardianUpgradeItem
{
  public GuardianUpgradeRange()
  {
    super(7, new ItemStack(net.minecraft.init.Items.bow), "guardian.upgrade.name.range", "guardian.upgrade.desc.range", 3, 1, 350, 450);
  }
  
  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  

  public void updateGolem(int level, EntityGolem golem)
  {
    if (golem.hasUpgrade(28)) {
      return;
    }
    if (golem.worldObj.rand.nextBoolean()) {
      ItemStack stack = golem.getStackInSlot(0);
      


      AxisAlignedBB axisalignedbb = AxisAlignedBB.getBoundingBox(golem.posX - 1.0D, golem.posY - 1.0D, golem.posZ - 1.0D, golem.posX + 1.0D, golem.posY + 1.0D, golem.posZ + 1.0D).expand(3 * level, 3 * level, 3 * level);
      java.util.List<EntityPlayer> list = golem.worldObj.getEntitiesWithinAABB(EntityPlayer.class, axisalignedbb);
      

      for (Object playerObj : list) {
        EntityPlayer player = (EntityPlayer)playerObj;
        if (player.getDistanceToEntity(golem) <= 3 * level) {
          boolean isAEnnemy = true;
          if ((golem.getStackInSlot(0) != null) && ((golem.getStackInSlot(0).getItem() instanceof ItemWhitelist)))
          {
            if ((golem.getStackInSlot(0).hasTagCompound()) && (golem.getStackInSlot(0).getTagCompound().hasKey("players")))
            {
              for (int i = 0; i < golem.getStackInSlot(0).getTagCompound().getTagList("players", 8).tagCount(); i++)
              {
                if (golem.getStackInSlot(0).getTagCompound().getTagList("players", 8).getStringTagAt(i).toLowerCase().equals(player.getGameProfile().getName().toString().toLowerCase()))
                {
                  isAEnnemy = false;
                }
              }
            }
          }
          if (golem.getOwner().equals(player.getGameProfile().getId().toString())) {
            isAEnnemy = false;
          }
          if (isAEnnemy) {
            player.attackEntityFrom(DamageSource.causeMobDamage(golem), (float)golem.getEntityAttribute(net.minecraft.entity.SharedMonsterAttributes.attackDamage).getBaseValue());
            break;
          }
        }
      }
    }
  }
}
