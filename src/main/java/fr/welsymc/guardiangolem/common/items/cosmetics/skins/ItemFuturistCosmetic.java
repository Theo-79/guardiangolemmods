package fr.welsymc.guardiangolem.common.items.cosmetics.skins;

import fr.welsymc.guardiangolem.common.items.ItemCosmetic;
import net.minecraft.util.ResourceLocation;

public class ItemFuturistCosmetic
  extends ItemCosmetic implements ICosmeticSkin
{
  private ResourceLocation texture = new ResourceLocation("guardiangolem", "textures/cosmetics/skins/futurist.png");
  
  public ItemFuturistCosmetic(String name) {
    super(name);
  }
  
  public ResourceLocation texture()
  {
    return this.texture;
  }
}
