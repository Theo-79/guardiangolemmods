package fr.welsymc.guardiangolem.common.items.guardianupgrades;

import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.utils.GuardianUtils;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.world.World;

public class GuardianUpgradeBeacon extends GuardianUpgradeItem
{
  public GuardianUpgradeBeacon()
  {
    super(1, new ItemStack(net.minecraft.init.Blocks.beacon), "guardian.upgrade.name.beacon", "guardian.upgrade.desc.beacon", 1, 1, 400, 300);
  }
  

  public ItemStack getItemStack(int level)
  {
    return this.stack;
  }
  
  public void updateGolem(int level, EntityGolem golem)
  {
    if (golem.worldObj.getWorldTime() % 80L == 0L) {
      GuardianUtils.setEffectToGolem(golem, 1, Potion.regeneration, true);
    }
  }
}
