package fr.welsymc.guardiangolem.common.inventories;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class InventoryGuardianKeeper implements net.minecraft.inventory.IInventory
{
  ItemStack[] content;
  
  public InventoryGuardianKeeper()
  {
    this.content = new ItemStack[9];
  }
  
  public int getSizeInventory()
  {
    return this.content.length;
  }
  
  public ItemStack getStackInSlot(int index)
  {
    return this.content[index];
  }
  
  public ItemStack decrStackSize(int slotIndex, int amount)
  {
    if (this.content[slotIndex] != null)
    {

      if (this.content[slotIndex].stackSize <= amount) {
        ItemStack itemstack = this.content[slotIndex];
        this.content[slotIndex] = null;
        markDirty();
        return itemstack;
      }
      ItemStack itemstack = this.content[slotIndex].splitStack(amount);
      
      if (this.content[slotIndex].stackSize == 0) {
        this.content[slotIndex] = null;
      }
      
      markDirty();
      return itemstack;
    }
    
    return null;
  }
  

  public void setInventorySlotContents(int index, ItemStack stack)
  {
    this.content[index] = stack;
  }
  
  public int getInventoryStackLimit()
  {
    return 1;
  }
  

  public void markDirty() {}
  

  public boolean isUseableByPlayer(EntityPlayer player)
  {
    return true;
  }
  
  public boolean isItemValidForSlot(int index, ItemStack stack)
  {
    return false;
  }
  

  public ItemStack getStackInSlotOnClosing(int p_70304_1_)
  {
    return null;
  }
  


  public String getInventoryName()
  {
    return "guardian.keeper.inventory";
  }
  


  public boolean hasCustomInventoryName()
  {
    return false;
  }
  
  public void openInventory() {}
  
  public void closeInventory() {}
}
