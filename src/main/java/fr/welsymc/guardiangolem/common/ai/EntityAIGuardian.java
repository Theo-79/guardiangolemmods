package fr.welsymc.guardiangolem.common.ai;

import com.mojang.authlib.GameProfile;
import fr.welsymc.guardiangolem.common.entities.EntityGolem;
import fr.welsymc.guardiangolem.common.items.ItemWhitelist;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class EntityAIGuardian extends EntityAIAttackOnCollide
{
  EntityGolem golem;
  
  public EntityAIGuardian(EntityGolem creature, double speedIn, boolean useLongMemory)
  {
    super(creature, speedIn, useLongMemory);
    this.golem = creature;
  }
  
  public boolean shouldExecute()
  {
    if (this.golem.hasUpgrade(28)) {
      return false;
    }
    EntityLivingBase base = this.golem.getAttackTarget();
    if (base == null)
      return false;
    if (((base instanceof EntityWither)) && (!this.golem.hasUpgrade(16)))
      return false;
    if ((base instanceof EntityPlayer)) {
      EntityPlayer player = (EntityPlayer)base;
      boolean isAEnnemy = true;
      if ((this.golem.getStackInSlot(0) != null) && ((this.golem.getStackInSlot(0).getItem() instanceof ItemWhitelist)))
      {
        if ((this.golem.getStackInSlot(0).hasTagCompound()) && (this.golem.getStackInSlot(0).getTagCompound().hasKey("players")))
        {
          for (int i = 0; i < this.golem.getStackInSlot(0).getTagCompound().getTagList("players", 8).tagCount(); i++)
          {
            if (this.golem.getStackInSlot(0).getTagCompound().getTagList("players", 8).getStringTagAt(i).toLowerCase().equals(((EntityPlayer)base).getGameProfile().getName().toString().toLowerCase()))
            {
              isAEnnemy = false;
            }
          }
        }
      }
      
      if (this.golem.getOwner().equals(((EntityPlayer)this.golem.getAttackTarget()).getGameProfile().getId().toString())) { isAEnnemy = false;
      }
      return isAEnnemy;
    }
    

    return super.shouldExecute();
  }
}
