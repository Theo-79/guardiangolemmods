package fr.welsymc.guardiangolem.init;

import cpw.mods.fml.common.registry.GameRegistry;
import fr.paladium.palamod.api.ItemsRegister;
import fr.welsymc.guardiangolem.common.init.ItemInit;
import fr.welsymc.guardiangolem.recipe.MiniGolemRecipe;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class CraftInit
{
  public CraftInit() {}
  
  public static void init()
  {
    GameRegistry.addRecipe(new ItemStack(ItemInit.GUARDIAN_STONE, 1), new Object[] { " X ", "XYX", " X ", Character.valueOf('X'), new ItemStack(net.minecraft.init.Blocks.stone), 
      Character.valueOf('Y'), new ItemStack(ItemsRegister.COMPRESSED_PALADIUM) });
    GameRegistry.addRecipe(new ItemStack(fr.welsymc.guardiangolem.common.init.BlockInit.GUARDIAN_KEEPER), new Object[] { "YYY", "ZXZ", "YYY", 
      Character.valueOf('Y'), ItemsRegister.PALADIUM_INGOT, Character.valueOf('Z'), new ItemStack(ItemsRegister.COMPRESSED_PALADIUM), 
      Character.valueOf('X'), ItemInit.GUARDIAN_STONE });
    GameRegistry.addRecipe(new ItemStack(fr.welsymc.guardiangolem.common.init.BlockInit.GUARDIAN_BLOCK), new Object[] { "YYY", "YXY", "YYY", 
      Character.valueOf('X'), ItemInit.GUARDIAN_STONE, Character.valueOf('Y'), ItemsRegister.TITANE_INGOT });
    
    GameRegistry.addRecipe(new ItemStack(ItemInit.GUARDIAN_RENAMER), new Object[] { " P ", "IPI", " P ", Character.valueOf('P'), Items.paper, Character.valueOf('I'), Items.stick });
    
    GameRegistry.addRecipe(new ItemStack(ItemInit.GUARDIAN_WHITELIST), new Object[] { "   ", "PPP", "B B", Character.valueOf('P'), Items.paper, Character.valueOf('B'), Items.book });
    
    GameRegistry.addRecipe(new ItemStack(ItemInit.GUARDIAN_HAMMER), new Object[] { "TPT", " S ", " S ", Character.valueOf('T'), ItemsRegister.TITANE_INGOT, Character.valueOf('P'), ItemsRegister.PALADIUM_INGOT, Character.valueOf('S'), Items.stick });
    
    MiniGolemRecipe miniGolemRecipe = new MiniGolemRecipe();
    miniGolemRecipe.init();
    GameRegistry.addRecipe(miniGolemRecipe);
  }
}
