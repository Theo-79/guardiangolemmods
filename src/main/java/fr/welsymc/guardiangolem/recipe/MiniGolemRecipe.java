package fr.welsymc.guardiangolem.recipe;

import fr.paladium.palamod.api.BlocksRegister;
import fr.welsymc.guardiangolem.common.init.ItemInit;
import java.util.HashMap;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class MiniGolemRecipe implements net.minecraft.item.crafting.IRecipe
{
  public MiniGolemRecipe() {}
  
  private HashMap<Integer, ItemStack> matrix = new HashMap();
  
  public void init() {
    this.matrix.put(Integer.valueOf(0), new ItemStack(BlocksRegister.BLOCK_PALADIUM));
    this.matrix.put(Integer.valueOf(1), new ItemStack(ItemInit.GUARDIAN_STONE));
    this.matrix.put(Integer.valueOf(2), new ItemStack(BlocksRegister.BLOCK_PALADIUM));
    this.matrix.put(Integer.valueOf(4), new ItemStack(BlocksRegister.BLOCK_PALADIUM));
  }
  
  public boolean matches(InventoryCrafting inventory, World world)
  {
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 3; j++) {
        if (!corresponds((ItemStack)this.matrix.get(Integer.valueOf(i * 3 + j)), inventory.getStackInSlot(i * 3 + j))) return false;
      }
    }
    return true;
  }
  
  public static boolean corresponds(ItemStack item, ItemStack stack)
  {
    if ((stack == null) && (item == null)) { return true;
    }
    if ((stack == null) && (item != null)) return false;
    if ((stack != null) && (item == null)) { return false;
    }
    if (stack.getItem() != item.getItem()) { return false;
    }
    return true;
  }
  
  public ItemStack getCraftingResult(InventoryCrafting inventory)
  {
    ItemStack stack = new ItemStack(ItemInit.MINI_GOLEM);
    if (!inventory.getStackInSlot(1).hasTagCompound()) {
      return null;
    }
    if (inventory.getStackInSlot(1).getItem() != ItemInit.GUARDIAN_STONE) {
      return null;
    }
    stack.setTagCompound(inventory.getStackInSlot(1).getTagCompound());
    return stack;
  }
  
  public int getRecipeSize()
  {
    return 9;
  }
  
  public ItemStack getRecipeOutput()
  {
    return new ItemStack(ItemInit.MINI_GOLEM);
  }
}
